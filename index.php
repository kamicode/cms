<?php
header('Content-type: text/html; charset=UTF-8');

session_start();

class Main {
    public static function run() {
        require_once('Utils.php');
        Utils::init($_GET, $_POST, $_SESSION, $_COOKIE);

        require_once('Bdd/config.php');
        require_once('Bdd/Bdd.class.php');
        require_once('Modules/Module.class.php');
        require_once('Modules/FrontOffice.class.php');
        require_once('Modules/Site/Front/Site.class.php');

        $site = new Site('');
        $site->action();
        $html = $site->affiche();
        $html = Utils::linearize($html);
        echo $html;
    }
}

Main::run();

?>

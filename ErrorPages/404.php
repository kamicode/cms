<?php

Class Error {
    public static function sendMail() {
        $to = 'contact@haiecapique.fr';
        $sujet = 'Erreur 404 sur le site "Haiecapique"';
        $headers = 'From: "Haiecapique"<contact@haiecapique.fr>'."\r\n";
        $headers .= 'Reply-To: contact@haiecapique.fr'."\r\n";
        $headers .= 'Content-Type: text/html; charset="utf-8"'."\r\n";
        $headers .= 'Content-Transfer-Encoding: 8bit'."\r\n";
        $message = 'Le site "Haiecapique" a subit une Erreur 404 :<br /><br />';
        $message .= 'Date: '.date("d/m/Y H:i").'<br />';
        $message .= 'Page concernée: '.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].'<br />';
        if (!empty($_SERVER['HTTP_REFERER'])) {
            $message .= 'Page précédente: '.$_SERVER['HTTP_REFERER'].'<br />';
        }
        $message .= 'Adresse IP du visiteur: '.$_SERVER['REMOTE_ADDR'].'<br />';
        $message .= 'User agent: '.$_SERVER['HTTP_USER_AGENT'];
        mail($to, $sujet, $message, $headers);
    }

    public static function run($embed) {
        $html = '';

        if(isset($embed) && $embed == true) {
            $html .= '<!DOCTYPE html>';
            $html .= '<head>';
            $html .= '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">';
            $html .= '<meta name="author" lang="fr" content="Tristan Kahn" />';
            $html .= '<meta name="publisher" content="Tristan Kahn" />';
            $html .= '<title>404 Error</title>';
            $html .= '<link rel="shortcut icon" href="/favicon.ico" />';
            $html .= '</head>';
            $html .= '<body>';
        }

        $html .= '<div align="center">';
        $html .= '<p><img src="/ErrorPages/404.gif" width="401" height="418" alt="404 Error" /></p>';
        $html .= '<p>Hum ... this is embarrassing, the file you requested was not found.<br />';

        $html .= 'An email has been sent to the webmaster of the site to fix the problem.</p>';
        sendMail();

        if (!empty($_SERVER['HTTP_REFERER'])) {
            $html .= '<p><a href="'.$_SERVER['HTTP_REFERER'].'">Back to previous page</a></p>';
        }
        $html .= '</div>';

        if(isset($embed) && $embed == true) {
            $html .= '</body>';
            $html .= '</html>';
        }
        echo $html;
    }
}


?>

<?php

abstract class BackOffice extends Module {

    public function init($nom = '', $racineSite = '') {
        parent::init($nom, $racineSite);
    }

    abstract protected function listeActions();
    abstract protected function listeModules();
}

?>

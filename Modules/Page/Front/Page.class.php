<?php

class Page extends FrontOffice {

    protected $livreOr;
    protected $contact;
    protected $search;

    public function __construct($racineSite, $search) {
        parent::init('Page', $racineSite);

        require_once($racineSite.'Modules/LivreOr/Front/LivreOr.class.php');
        $this->livreOr = new LivreOr($racineSite);

        require_once($racineSite.'Modules/Contact/Front/Contact.class.php');
        $this->contact = new Contact($racineSite);

        $this->search = $search;
    }

    public function action() {
        switch($this->action) {
            default: break;
        }
        $this->contact->action();
        $this->livreOr->action();
    }

    public function affiche() {
        $retour = '<div id="page">';
        switch($this->affiche) {
            default: $retour .= $this->afficheDefaut(); break;
        }
        $retour .= '</div>';
        return $retour;
    }

    public function title() {
        $req = Bdd::execute('SELECT * FROM page WHERE nom = ?', array(Utils::$GET['page']));
        $donnees = $req->fetch();
        return html_entity_decode($donnees['title-'.Utils::$GET['langue']]);
    }

    public function keywords() {
        $req = Bdd::execute('SELECT *  FROM page WHERE nom = ?', array(Utils::$GET['page']));
        $donnees = $req->fetch();
        return html_entity_decode($donnees['keywords-'.Utils::$GET['langue']]);
    }

    public function description() {
        $req = Bdd::execute('SELECT * FROM page WHERE nom = ?', array(Utils::$GET['page']));
        $donnees = $req->fetch();
        return html_entity_decode($donnees['description-'.Utils::$GET['langue']]);
    }

    protected function afficheDefaut() {
        $retour = '';
        if(Utils::$GET['page'] == "search") {
            $retour .= $this->search->afficheResult();
        }
        else {
            $req = Bdd::execute('SELECT * FROM page WHERE nom = ?', array(Utils::$GET['page']));
            $donnees = $req->fetch();
            $retour .= html_entity_decode($donnees['contenu-'.Utils::$GET['langue']]);
            $req->closeCursor();
            if($retour == "") {
                ob_start();
                require_once($this->racineSite . 'ErrorPages/404.php');
                Error::run(false);
                $retour = ob_get_clean();
            }

            if(Utils::$GET['page'] == "contact") {
                $retour .= $this->contact->affiche();
            }
            else if(Utils::$GET['page'] == "livreor") {
                $retour .= $this->livreOr->affiche();
            }
        }
        return $retour;
    }

    protected function css() {
        $fichiers = array();
        foreach($this->contact->css() as $fic) {
            $fichiers[] = $fic;
        }
        foreach($this->livreOr->css() as $fic) {
            $fichiers[] = $fic;
        }
        return $fichiers;
    }

    protected function script() {
        $fichiers = array();
        foreach($this->contact->script() as $fic) {
            $fichiers[] = $fic;
        }
        foreach($this->livreOr->script() as $fic) {
            $fichiers[] = $fic;
        }
        return $fichiers;
    }
}

?>

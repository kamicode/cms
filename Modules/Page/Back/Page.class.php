<?php

class Page extends BackOffice {

    protected $livreOr;

    public function __construct($racineSite) {
        parent::init('Page', $racineSite);

        require_once($racineSite.'Modules/LivreOr/Back/LivreOr.class.php');
        $this->livreOr = new LivreOr($racineSite);
    }

    public function action() {
        switch($this->action) {
            case 'creer': $this->creer(); break;
            case 'editer': $this->editer(); break;
            case 'supprimer': $this->supprimer(); break;
            default: break;
        }
        $this->livreOr->action();
    }

    protected function creer() {
        $req = 'INSERT INTO page (`id`, `nom`, `title-fr`, `title-en`, `keywords-fr`, `keywords-en`, `description-fr`, `description-en`, `contenu-fr`, `contenu-en`)';
        $req .= ' VALUES (NULL , "'.htmlentities(Utils::$POST['nomPage']).'", "';
        $req .= htmlentities(Utils::$POST['title-fr']).'", "'.htmlentities(Utils::$POST['title-en']).'", "';
        $req .= htmlentities(Utils::$POST['keywords-fr']).'", "'.htmlentities(Utils::$POST['keywords-en']).'", "';
        $req .= htmlentities(Utils::$POST['description-fr']).'", "'.htmlentities(Utils::$POST['description-en']).'", "';
        $req .= htmlentities(Utils::$POST['contenuPage']).'", "'.htmlentities(Utils::$POST['traductionPage']).'")';
        $query = Bdd::execute($req, array());
        $query->closeCursor();
        $this->affiche = 'Lister';
    }

    protected function editer() {
        $req = 'UPDATE page SET ';
        $req .= '`nom` = "'.htmlentities(Utils::$POST['nomPage']).'", ';
        $req .= '`title-fr` = "'.htmlentities(Utils::$POST['title-fr']).'", ';
        $req .= '`title-en` = "'.htmlentities(Utils::$POST['title-en']).'", ';
        $req .= '`keywords-fr` = "'.htmlentities(Utils::$POST['keywords-fr']).'", ';
        $req .= '`keywords-en` = "'.htmlentities(Utils::$POST['keywords-en']).'", ';
        $req .= '`description-fr` = "'.htmlentities(Utils::$POST['description-fr']).'", ';
        $req .= '`description-en` = "'.htmlentities(Utils::$POST['description-en']).'", ';
        $req .= '`contenu-fr` = "'.htmlentities(Utils::$POST['contenuPage']).'", ';
        $req .= '`contenu-en` = "'.htmlentities(Utils::$POST['traductionPage']).'" ';
        $req .= ' WHERE `id` = '.htmlentities(Utils::$POST['idPage']);
        $query = Bdd::execute($req, array());
        $query->closeCursor();
        $this->affiche = 'Lister';
    }

    protected function supprimer() {
        $req = Bdd::execute('DELETE FROM page WHERE id = '.htmlentities(Utils::$POST['idPage']), array());
        $req->closeCursor();
        $this->affiche = 'Lister';
    }

    public function affiche() {
        $retour = '<div id="pageBack">';
        switch($this->affiche) {
            case 'Lister': $retour .= $this->afficheListe(); break;
            case 'Creer': $retour .= $this->afficheCreer(); break;
            case 'Editer': $retour .= $this->afficheEditer(); break;
            default: $retour .= $this->afficheDefaut(); break;
        }
        $retour .= '</div>';
        return $retour;
    }

    protected function afficheDefaut() {
        return 'affichage par defaut du module '.$this->nom.' coté admin';
    }

    protected function afficheListe() {
        $req = Bdd::execute('SELECT * FROM page ORDER BY `nom` ASC', array());
        $retour = '<table id="listePage">';
        $retour .= '<tr>';
        $retour .= '<th>Nom de la page</th>';
        $retour .= '<th>Editer la page</th>';
        $retour .= '<th>Supprimer la page</th>';
        $retour .= '<th>Voir la page</th>';
        $retour .= '<th>Voir la traduction</th>';
        $retour .= '</tr>';

        if($req->rowCount() > 0) {
            while ($donnees = $req->fetch()) {
                $retour .= '<tr>';
                $retour .= '<td>'.$donnees['nom'].'</td>';
                // Edition
                $retour .= '<td><form action="" method="post">';
                $retour .= '<p><input type="hidden" name="nom" value="'.$this->getNom().'" />';
                $retour .= '<input type="hidden" name="affiche" value="Editer" />';
                $retour .= '<input type="hidden" name="idPage" value="'.$donnees['id'].'" />';
                $retour .= '<input alt="Editer" src="'.$this->racineSite.'Images/editer.png" type="image" /></p></form></td>';
                // Suppression
                $retour .= '<td><form action="" method="post" onsubmit="return confirm(\'Etes-vous sur?\');">';
                $retour .= '<p><input type="hidden" name="nom" value="'.$this->getNom().'" />';
                $retour .= '<input type="hidden" name="action" value="supprimer" />';
                $retour .= '<input type="hidden" name="idPage" value="'.$donnees['id'].'" />';
                $retour .= '<input alt="Supprimer" src="'.$this->racineSite.'Images/delete.png" type="image"/></p></form></td>';
                // Apercu
                $retour .= '<td><form action="../'.$donnees['nom'].'-fr.html" method="post" target="_blank">';
                $retour .= '<p><input alt="Apercu" src="'.$this->racineSite.'Images/right.png" type="image"/></p></form></td>';
                // Apercu trad
                $retour .= '<td><form action="../'.$donnees['nom'].'-en.html" method="post" target="_blank">';
                $retour .= '<p><input alt="Apercu trad" src="'.$this->racineSite.'Images/right.png" type="image"/></p></form></td>';
                $retour .= '</tr>';
            }
        }
        $retour .= '</table>';
        $req->closeCursor();
        return $retour;
    }

    protected function afficheCreer() {
        $retour = '<form action="" method="post">';
        $retour .= '<p><input type="hidden" name="nom" value="'.$this->getNom().'" />';
        $retour .= '<input type="hidden" name="action" value="creer" />';

        $retour .= '<label for="nomPage">Nom de la page : </label>';
        $retour .= '<input type="text" name="nomPage" id="nomPage" value="" size="40" maxlength="40" /></p>';

        $retour .= '<div id="tabs">';
        $retour .= '<ul>';
        $retour .= '<li><a href="#tabs-fr">Français</a></li>';
        $retour .= '<li><a href="#tabs-en">Anglais</a></li>';
        $retour .= '</ul>';

        $retour .= '<div id="tabs-fr">';
        $retour .= '<p><label for="title-fr">Titre de la page : </label><input type="text" name="title-fr" id="title-fr" value="" /><br />';
        $retour .= '<label for="keywords-fr">Keywords de la page : </label><input type="text" name="keywords-fr" id="keywords-fr" value="" /><br />';
        $retour .= '<label for="description-fr">Description de la page : </label><input type="text" name="description-fr" id="description-fr" value="" maxlength="400" /></p>';
        $retour .= '<p><textarea cols="80" name="contenuPage" id="contenuPage" rows="10"></textarea></p>';
        $retour .= '</div>';

        $retour .= '<div id="tabs-en">';
        $retour .= '<p><label for="title-en">Titre de la page : </label><input type="text" name="title-en" id="title-en" value="" /><br />';
        $retour .= '<label for="keywords-en">Keywords de la page : </label><input type="text" name="keywords-en" id="keywords-en" value="" /><br />';
        $retour .= '<label for="description-en">Description de la page : </label><input type="text" name="description-en" id="description-en" value="" maxlength="400" /></p>';
        $retour .= '<p><textarea cols="80" name="traductionPage" id="traductionPage" rows="10"></textarea></p>';
        $retour .= '</div>';
        $retour .= '</div>';

        $retour .= '<p><input alt="Valider" src="'.$this->racineSite.'Images/valider.png" type="image" /></p></form>';
        return $retour;
    }

    protected function afficheEditer() {
        $req = Bdd::execute('SELECT * FROM page WHERE id = '.htmlentities(Utils::$POST['idPage']), array());
        $donnees = $req->fetch();

        $retour = '<form action="" method="post">';
        $retour .= '<p><input type="hidden" name="nom" value="'.$this->getNom().'" />';
        $retour .= '<input type="hidden" name="idPage" value="'.htmlentities(Utils::$POST['idPage']).'" />';
        $retour .= '<input type="hidden" name="action" value="editer" />';
        $retour .= '<label for="nomPage">Nom de la page : </label>';
        $retour .= '<input type="text" name="nomPage" id="nomPage" value="'.$donnees['nom'].'" size="40" maxlength="40" /></p>';

        $retour .= '<div id="tabs">';
        $retour .= '<ul>';
        $retour .= '<li><a href="#tabs-fr">Français</a></li>';
        $retour .= '<li><a href="#tabs-en">Anglais</a></li>';
        $retour .= '</ul>';

        $retour .= '<div id="tabs-fr">';
        $retour .= '<p><label for="title-fr">Titre de la page : </label><input type="text" name="title-fr" id="title-fr" value="'.html_entity_decode($donnees['title-fr']).'" /><br />';
        $retour .= '<label for="keywords-fr">Keywords de la page : </label><input type="text" name="keywords-fr" id="keywords-fr" value="'.html_entity_decode($donnees['keywords-fr']).'" /><br />';
        $retour .= '<label for="description-fr">Description de la page : </label><input type="text" name="description-fr" id="description-fr" value="'.html_entity_decode($donnees['description-fr']).'" maxlength="400" /></p>';
        $retour .= '<p><textarea cols="80" name="contenuPage" id="contenuPage" rows="10">'.html_entity_decode($donnees['contenu-fr']).'</textarea></p>';
        $retour .= '</div>';

        $retour .= '<div id="tabs-en">';
        $retour .= '<p><label for="title-en">Titre de la page : </label><input type="text" name="title-en" id="title-en" value="'.html_entity_decode($donnees['title-en']).'" /><br />';
        $retour .= '<label for="keywords-en">Keywords de la page : </label><input type="text" name="keywords-en" id="keywords-en" value="'.html_entity_decode($donnees['keywords-en']).'" /><br />';
        $retour .= '<label for="description-en">Description de la page : </label><input type="text" name="description-en" id="description-en" value="'.html_entity_decode($donnees['description-en']).'" maxlength="400" /></p>';
        $retour .= '<p><textarea cols="80" name="traductionPage" id="traductionPage" rows="10">'.html_entity_decode($donnees['contenu-en']).'</textarea></p>';
        $retour .= '</div>';
        $retour .= '</div>';

        $retour .= '<p><input alt="Valider" src="'.$this->racineSite.'Images/valider.png" type="image" /></p></form>';
        return $retour;
    }

    protected function listeActions() {
        $retour = array();
        $retour[] = 'Lister';
        $retour[] = 'Creer';
        return $retour;
    }

    protected function listeModules() {
        $retour = array();
        foreach($this->livreOr->listeModules() as $element) {
            $retour[] = $element;
        }
        $retour[] = $this;
        return $retour;
    }

    protected function css() {
        $fichiers = array();
        foreach($this->livreOr->css() as $fic) {
            $fichiers[] = $fic;
        }
        return $fichiers;
    }

    protected function script() {
        $fichiers = array();
        foreach($this->livreOr->script() as $fic) {
            $fichiers[] = $fic;
        }
        $fichiers[] = $this->racineSite.'Modules/'.$this->nom.'/Back/Scripts/script.js';
        return $fichiers;
    }
}

?>

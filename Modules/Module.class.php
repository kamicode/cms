<?php

abstract class Module {

    protected $nom;
    protected $action;
    protected $affiche;
    protected $racineSite;

    public function init($nom = '', $racineSite = '') {
        $this->nom = $nom;
        $this->racineSite = $racineSite;

        $this->action = '';
        $this->affiche = '';
        if(isset(Utils::$POST['nom']) && htmlentities(Utils::$POST['nom']) == $nom) {
            if(isset(Utils::$POST['action'])) {
                $this->action =  htmlentities(Utils::$POST['action']);
            }
            if(isset(Utils::$POST['affiche'])) {
                $this->affiche =  htmlentities(Utils::$POST['affiche']);
            }
        }
    }

    public function getNom() {
        return $this->nom;
    }

    public function getAction() {
        return $this->action;
    }

    public function getAffiche() {
        return $this->affiche;
    }

    public function getRacineSite() {
        return $this->racineSite;
    }

    public function setNom($nom) {
        $this->nom = $nom;
    }

    public function setAction($action) {
        $this->action = $action;
    }

    public function setAffiche($affiche) {
        $this->affiche = $affiche;
    }

    public function setRacineSite($racineSite) {
        $this->racineSite= $racineSite;
    }

    protected function css() {
        return array();
    }

    protected function script() {
        return array();
    }

    abstract public function action();
    abstract public function affiche();
    abstract protected function afficheDefaut();
}

?>

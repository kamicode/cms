<?php

class LivreOr extends FrontOffice {

    protected $nombreDeMessagesParPage;
    
    public function __construct($racineSite) {
        parent::init('LivreOr', $racineSite);
        $this->nombreDeMessagesParPage = 4;
    }

    public function action() {
        switch($this->action) {
            case 'Ajouter': $this->ajouter(); break;
            default: break;
        }
    }

    protected function ajouter() {
        if(isset(Utils::$POST['pseudo']) && isset(Utils::$POST['commentaire']) && Utils::$POST['pseudo'] != "" && Utils::$POST['commentaire'] != "") {
            $req = Bdd::execute('INSERT INTO livreor (`id` , `date`, `pseudo` , `commentaire`, `actif`) VALUES (NULL , "'.date('d-m-Y à H:i:s').'", "'.htmlentities(Utils::$POST['pseudo']).'", "'.htmlentities(nl2br(Utils::$POST['commentaire'])).'", "1")', array());
        $req->closeCursor();

            $headers ='From: "Haiecapique"<contact@haiecapique.fr>'."\r\n";
            $headers .='Reply-To: contact@haiecapique.fr'."\r\n";
            $headers .='Content-Type: text/html; charset="utf-8"'."\r\n";
            $headers .='Content-Transfer-Encoding: 8bit'."\r\n";
            $to = 'contact@haiecapique.fr';
            $sujet = 'Livre d\'or d\'haiecapique';
            $message = 'Un commentaire a été posté sur le site "haiecapique.fr" : <br />';
            $message .= 'Le '.date('d-m-Y à H:i:s').', '.htmlentities(stripslashes(Utils::$POST['pseudo'])).' à écrit :<br />';
            $message .= nl2br(stripslashes(Utils::$POST['commentaire']));
            mail($to, $sujet, $message, $headers);
        }
        $this->affiche = 'AjoutOk';
    }

    public function affiche() {
        $retour = '<div id="livreOr">';
        switch($this->affiche) {
            case 'Ajouter': $retour .= $this->afficheAjouter(); break;
            case 'AjoutOk': $retour .= $this->afficheAjouterOk(); break;
            default: $retour .= $this->afficheDefaut(); break;
        }
        $retour .= '</div>';
        return $retour;
    }

    protected function afficheAjouterOk() {
        $retour = 'Commentaire enregistré<br />';
        $retour .= $this->afficheDefaut();
        return $retour;
    }

    protected function afficheAjouter() {
        $retour = '<form action="" method="post">';
        $retour .= '<p><input type="hidden" name="nom" value="'.$this->getNom().'" />';
        $retour .= '<input type="hidden" name="action" value="Ajouter" />';
        if(Utils::$GET['langue'] == 'fr') {
            $retour .= '<label for="pseudo">Pseudo *: </label><br />';
        }
        else if (Utils::$GET['langue'] == 'en') {
            $retour .= '<label for="pseudo">Pseudo *: </label><br />';
        }
        $retour .= '<input type="text" name="pseudo" id="pseudo" value="" required /></p>';
        if(Utils::$GET['langue'] == 'fr') {
            $retour .= '<label for="commentaire">Commentaire *: </label><br />';
        }
        else if (Utils::$GET['langue'] == 'en') {
            $retour .= '<label for="commentaire">Comment *: </label><br />';
        }
        $retour .= '<textarea name="commentaire" id="commentaire" value="" cols="80" rows="10" required></textarea></p>';
        $retour .= '<p><input alt="Valider" src="'.$this->racineSite.'Images/valider.png" type="image" /></p></form>';
        return $retour;
    }
    
    protected function afficheLiensPages() {
        $req = Bdd::execute('SELECT COUNT(*) AS nb_messages FROM livreor WHERE actif = 1', array());
        $retour = '';
        $donnees = $req->fetch();
        $totalDesMessages = $donnees['nb_messages'];
        $nombreDePages  = ceil($totalDesMessages / $this->nombreDeMessagesParPage);
        $retour .= '<p>Page : ';
        for ($i = 1 ; $i <= $nombreDePages ; $i++) {
            $retour .= '&nbsp;<a href="livreor-fr.html?currentPage='.$i.'">'.$i.'</a>&nbsp;';
        }
        $retour .= '</p>';
        return $retour;
    }

    protected function afficheDefaut() {
        $page = 1;
        if(isset(Utils::$GET['currentPage'])) {
            $page =  htmlentities(Utils::$GET['currentPage']);
        }

        $premierMessageAafficher = ($page - 1) * $this->nombreDeMessagesParPage;

        $params = array(
            array($premierMessageAafficher, PDO::PARAM_INT),
            array($this->nombreDeMessagesParPage, PDO::PARAM_INT)
        );

        $req = Bdd::executerWithType('SELECT * FROM livreor WHERE actif = 1 ORDER BY id DESC LIMIT ?,?' , $params);
        $retour = '';

        $retour .= $this->afficheLiensPages();
        
        $retour .= '<form action="" method="post">';
        $retour .= '<p><input type="hidden" name="nom" value="'.$this->getNom().'" />';
        $retour .= '<input type="hidden" name="affiche" value="Ajouter" />';
        if(Utils::$GET['langue'] == 'fr') {
            $retour .= '<input type="submit" value="Ajouter un commentaire" /></p></form>';
        }
        else if (Utils::$GET['langue'] == 'en') {
            $retour .= '<input type="submit" value="Add comment" /></p></form>';
        }
        
        if($req->rowCount() != 0) {
            while ($donnees = $req->fetch()) {
                $retour .= '<table id="listeComms">';
                $retour .= '<tr>';
                if(Utils::$GET['langue'] == 'fr') {
                    $retour .= '<th>Le '.$donnees['date'].' , '.$donnees['pseudo'].' a écrit :</th>';
                }
                else if (Utils::$GET['langue'] == 'en') {
                    $retour .= '<th>'.$donnees['date'].' , '.$donnees['pseudo'].' wrote :</th>';
                }
                $retour .= '</tr>';
                $retour .= '<tr>';
                $retour .= '<td>'.html_entity_decode($donnees['commentaire']).'</td>';
                $retour .= '</tr>';
                $retour .= '</table>';
            }
        }
        $retour .= '<form action="" method="post">';
        $retour .= '<p><input type="hidden" name="nom" value="'.$this->getNom().'" />';
        $retour .= '<input type="hidden" name="affiche" value="Ajouter" />';
        if(Utils::$GET['langue'] == 'fr') {
            $retour .= '<input type="submit" value="Ajouter un commentaire" /></p></form>';
        }
        else if (Utils::$GET['langue'] == 'en') {
            $retour .= '<input type="submit" value="Add comment" /></p></form>';
        }
        
        $retour .= $this->afficheLiensPages();

        $req->closeCursor();
        return $retour;
    }
}

?>

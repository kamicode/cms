<?php

class LivreOr extends BackOffice {

    public function __construct($racineSite) {
        parent::init('LivreOr', $racineSite);
    }

    public function action() {
        switch($this->action) {
            case 'Desactiver': $this->desactiver(); break;
            case 'Activer': $this->activer(); break;
            case 'Supprimer': $this->supprimer(); break;
            default: break;
        }
    }

    protected function desactiver() {
        $req = Bdd::execute('UPDATE livreor SET actif = ? WHERE id = ?', array(0, htmlentities(Utils::$POST['idComms'])));
        $req->closeCursor();
    }

    protected function activer() {
        $req = Bdd::execute('UPDATE livreor SET actif = ? WHERE id = ?', array(1, htmlentities(Utils::$POST['idComms'])));
        $req->closeCursor();
    }

    protected function supprimer() {
        $req = Bdd::execute('DELETE FROM livreor WHERE id = ?', array(htmlentities(Utils::$POST['idComms'])));
        $req->closeCursor();
    }

    public function affiche() {
        $retour = '<div id="livreOrBack">';
        switch($this->affiche) {
            default: $retour .= $this->afficheDefaut(); break;
        }
        $retour .= '</div>';
        return $retour;
    }

    protected function afficheDefaut() {
        $req = Bdd::execute('SELECT * FROM livreor ORDER BY id DESC', array());
        $retour = '';

        $retour .= '<table id="listeComms">';
        $retour .= '<tr><th>Pseudo</th><th>Commentaire</th><th>Etat</th><th>Supprimer</th></tr>';
        while ($donnees = $req->fetch()) {
            $retour .= '<tr>';
            $retour .= '<td>Le '.$donnees['date'].' , '.$donnees['pseudo'].' a écrit :</td>';
            $retour .= '<td>'.html_entity_decode($donnees['commentaire']).'</td>';

            $retour .= '<td><form action="" method="post">';
            $retour .= '<p><input type="hidden" name="nom" value="'.$this->getNom().'" />';
            $retour .= '<input type="hidden" name="idComms" value="'.$donnees['id'].'" />';
            if($donnees['actif'] == 1) {
                $retour .= '<input type="hidden" name="action" value="Desactiver" />';
                $retour .= '<input alt="Desactiver" src="'.$this->racineSite.'Images/publish.png" type="image" />';
            }
            else {
                $retour .= '<input type="hidden" name="action" value="Activer" />';
                $retour .= '<input alt="Activer" src="'.$this->racineSite.'Images/unpublish.png" type="image" />';
            }
            $retour .= '</p></form></td>';

            $retour .= '<td><form action="" method="post" onsubmit="return confirm(\'Etes-vous sur?\');">';
            $retour .= '<p><input type="hidden" name="nom" value="'.$this->getNom().'" />';
            $retour .= '<input type="hidden" name="idComms" value="'.$donnees['id'].'" />';
            $retour .= '<input type="hidden" name="action" value="Supprimer" />';
            $retour .= '<input type="submit" value="Supprimer" />';
            $retour .= '</p></form></td>';

            $retour .= '</tr>';
        }
        $retour .= '</table>';

        $req->closeCursor();
        return $retour;
    }

    protected function listeActions() {
        $retour = array();
        $retour[] = 'Voir';
        return $retour;
    }

    protected function listeModules() {
        $retour = array();
        $retour[] = $this;
        return $retour;
    }
}

?>

<?php

class MenuHorizontal extends FrontOffice {

    protected $index;

    public function __construct($racineSite) {
        parent::init('MenuHorizontal', $racineSite);
        $this->index = 0;
    }

    public function action() {
        switch($this->action) {
            default: break;
        }
    }

    public function affiche() {
        $retour = '<div id="menuHorizontal">';
        switch($this->affiche) {
            default: $retour .= $this->afficheDefaut(); break;
        }
        $retour .= '</div>';
        return $retour;
    }

    protected function afficheDefaut() {
        $retour = '<table>';

        $retour .= '<tr>';
        $req = Bdd::execute('SELECT * FROM menuhorizontal ORDER BY ordre ASC', array());
        $nb = $req->rowCount();
        $i = 1;
        while ($donnees = $req->fetch()) {
            $this->index = $this->index + 1;
            if($i == $nb) {
                // dernier
                $retour .= '<td class="dernier" id="menuHorizontal-'.$this->index.'">';
            }
            else {
                $retour .= '<td id="menuHorizontal-'.$this->index.'">';
            }
            $retour .= '<a href="/'.$donnees['lien'].'.html">'.$donnees['nom-'.Utils::$GET['langue']].'</a></td>';
            $i++;
        }
        $req->closeCursor();

        $retour .= '</tr>';
        $retour .= '</table>';
        return $retour;
    }
}

?>

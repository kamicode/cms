<?php

class MenuHorizontal extends BackOffice {

    public function __construct($racineSite) {
        parent::init('MenuHorizontal', $racineSite);
    }

    public function action() {
        switch($this->action) {
            case 'editer': $this->editer(); break;
            case 'supprimer': $this->supprimer(); break;
            case 'creer': $this->creer(); break;
            case 'descendre': $this->descendre(); break;
            case 'monter': $this->monter(); break;
            default: break;
        }
    }

    protected function descendre() {
        $req = Bdd::execute('UPDATE menuhorizontal SET ordre = "'.htmlentities(Utils::$POST['ordreMenu']).'" WHERE ordre='.(htmlentities(Utils::$POST['ordreMenu']) + 1), array());
        $req->closeCursor();
        $req = Bdd::execute('UPDATE menuhorizontal SET ordre = "'.(htmlentities(Utils::$POST['ordreMenu']) + 1).'" WHERE id = '.htmlentities(Utils::$POST['idMenu']), array());
        $req->closeCursor();
    }

    protected function monter() {
        $req = Bdd::execute('UPDATE menuhorizontal SET ordre = "'.htmlentities(Utils::$POST['ordreMenu']).'" WHERE ordre='.(htmlentities(Utils::$POST['ordreMenu']) - 1), array());
        $req->closeCursor();
        $req = Bdd::execute('UPDATE menuhorizontal SET ordre = "'.(htmlentities(Utils::$POST['ordreMenu']) - 1).'" WHERE id = '.htmlentities(Utils::$POST['idMenu']), array());
        $req->closeCursor();
    }

    protected function creer() {
        $req = Bdd::execute('INSERT INTO menuhorizontal (`id` , `nom-fr`, `nom-en`, `lien`, `ordre`) VALUES (NULL , "'.htmlentities(Utils::$POST['nomMenuFR']).'", "'.htmlentities(Utils::$POST['nomMenuEN']).'", "'.htmlentities(Utils::$POST['lienMenu']).'", "'.htmlentities(Utils::$POST['ordreMenu']).'")', array());
        $req->closeCursor();
        $this->affiche = 'liste';
    }

    protected function editer() {
        $req = Bdd::execute('UPDATE menuhorizontal SET `nom-fr` = "'.htmlentities(Utils::$POST['nomMenuFR']).'", `nom-en` = "'.htmlentities(Utils::$POST['nomMenuEN']).'", `lien` = "'.htmlentities(Utils::$POST['lienMenu']).'" WHERE id = '.htmlentities(Utils::$POST['idMenu']), array());
        $req->closeCursor();
    }

    protected function supprimer() {
        $id = htmlentities(Utils::$POST['idMenu']);
        // On selection mon ordre
        $req = Bdd::execute('SELECT ordre FROM menuhorizontal WHERE id = ?', array($id));
        $donnees = $req->fetch();
        $ordre = $donnees['ordre'];
        $req->closeCursor();

        // On modifie l'ordre de tous nos petit freres
        $req = Bdd::execute('SELECT id FROM menuhorizontal WHERE ordre > ? ORDER BY `ordre`', array($ordre));
        while ($donnees = $req->fetch()) {
            $req2 = Bdd::execute('UPDATE menuhorizontal SET ordre = ? WHERE id = ?', array($ordre, $donnees['id']));
            $req2->closeCursor();
            $ordre++;
        }
        $req->closeCursor();
        $req = Bdd::execute('DELETE FROM menuhorizontal WHERE id = ?', array($id));
        $req->closeCursor();
    }

    public function affiche() {
        $retour = '<div id="menuHorizontalBack">';
        switch($this->affiche) {
            default: $retour .= $this->afficheDefaut(); break;
        }
        $retour .= '</div>';
        return $retour;
    }

    protected function afficheDefaut() {
        $retour = $this->menu();
        return $retour;
    }

    protected function menu() {
        $retour = "";
        $req = Bdd::execute('SELECT * FROM menuhorizontal ORDER BY ordre', array());

        $retour .= '<ul>';
        $nb_item = $req->rowCount();
        $i = 0;
        while ($donnees = $req->fetch()) {
            $i++;
            // Edition
            $retour .= '<li>';
            $retour .= '<form action="" method="post">';
            $retour .= '<div><input type="hidden" name="nom" value="'.$this->getNom().'" />';
            $retour .= '<input type="hidden" name="action" value="editer" />';
            $retour .= '<input type="hidden" name="idMenu" value="'.$donnees['id'].'" />';
            $retour .= '<label for="nomMenuFR'.$i.'">nom : </label><input type="text" name="nomMenuFR" id="nomMenuFR'.$i.'" value="'.$donnees['nom-fr'].'" size="30" maxlength="30" />';
            $retour .= '<label for="nomMenuEN'.$i.'"> traduction : </label><input type="text" name="nomMenuEN" id="nomMenuEN'.$i.'" value="'.$donnees['nom-en'].'" size="30" maxlength="30" />';
            $retour .= '<label for="lienMenu'.$i.'"> lien :</label><select name="lienMenu" id="lienMenu'.$i.'" style="width:100px">';
            $req2 = Bdd::execute('SELECT nom FROM page ORDER BY nom', array());
            while ($donnees2 = $req2->fetch()) {
                if($donnees2['nom'] == $donnees['lien']) {
                    $retour .= '<option value="'.$donnees2['nom'].'" selected="selected">'.$donnees2['nom'].'</option>';
                }
                else {
                    $retour .= '<option value="'.$donnees2['nom'].'">'.$donnees2['nom'].'</option>';
                }
            }
            $rep = $this->racineSite."Applications/";
            $dir = opendir($rep);
            while ($f = readdir($dir)) {
                if(is_dir($rep.$f) && $f != "." && $f != "..") {
                    if("Applications/".$f == $donnees['lien']) {
                        $retour .= '<option value="Applications/'.$f.'" selected="selected">'.$f.'</option>';
                    } else {
                        $retour .= '<option value="Applications/'.$f.'">'.$f.'</option>';
                    }
                }
            }
            closedir($dir);

            $req2->closeCursor();
            $retour .= '</select>';
            $retour .= '<input type="submit" value="Editer" /></div></form>';

            // Ordre
            if($nb_item != 0) {
                if($donnees['ordre'] != $nb_item) {
                    $retour .= '<form action="" method="post">';
                    $retour .= '<div><input type="hidden" name="nom" value="'.$this->getNom().'" />';
                    $retour .= '<input type="hidden" name="action" value="descendre" />';
                    $retour .= '<input type="hidden" name="idMenu" value="'.$donnees['id'].'" />';
                    $retour .= '<input type="hidden" name="ordreMenu" value="'.$donnees['ordre'].'" />';
                    $retour .= '<input alt="" src="'.$this->racineSite.'Images/down.png" type="image" /></div></form>';
                }
                if($donnees['ordre'] != 1) {
                    $retour .= '<form action="" method="post">';
                    $retour .= '<div><input type="hidden" name="nom" value="'.$this->getNom().'" />';
                    $retour .= '<input type="hidden" name="action" value="monter" />';
                    $retour .= '<input type="hidden" name="idMenu" value="'.$donnees['id'].'" />';
                    $retour .= '<input type="hidden" name="ordreMenu" value="'.$donnees['ordre'].'" />';
                    $retour .= '<input alt="" src="'.$this->racineSite.'Images/up.png" type="image" /></div></form>';
                }
            }

            // Suppression
            $retour .= '<form action="" method="post" onsubmit="return confirm(\'Etes-vous sur?\');">';
            $retour .= '<div><input type="hidden" name="nom" value="'.$this->getNom().'" />';
            $retour .= '<input type="hidden" name="action" value="supprimer" />';
            $retour .= '<input type="hidden" name="idMenu" value="'.$donnees['id'].'" />';
            $retour .= '<input type="submit" value="Supprimer" /></div></form>';
            $retour .= '</li>';
        }

        $retour .= '<li>';
        $retour .= '<form action="" method="post">';
        $retour .= '<div><input type="hidden" name="nom" value="'.$this->getNom().'" />';
        $retour .= '<input type="hidden" name="action" value="creer" />';
        $buf = 0;
        $req2 = Bdd::execute('SELECT max(ordre) AS o FROM menuhorizontal', array());
        while ($donnees2 = $req2->fetch()) {
            $buf = $donnees2['0'] + 1;
        }
        $req2->closeCursor();
        $retour .= '<input type="hidden" name="ordreMenu" value="'.$buf.'" />';
        $retour .= '<label for="nomMenuFRCreateB'.$i.'">nom : </label><input type="text" name="nomMenuFR" id="nomMenuFRCreateB'.$i.'" value="" size="30" maxlength="30" />';
        $retour .= '<label for="nomMenuENCreateB'.$i.'"> traduction : </label><input type="text" name="nomMenuEN" id="nomMenuENCreateB'.$i.'" value="" size="30" maxlength="30" />';
        $retour .= '<label for="lienMenuCreateB'.$i.'"> lien :</label><select name="lienMenu" id="lienMenuCreateB'.$i.'" style="width:100px">';
        $req2 = Bdd::execute('SELECT nom FROM page ORDER BY nom', array());
        while ($donnees2 = $req2->fetch()) {
            $retour .= '<option value="'.$donnees2['nom'].'">'.$donnees2['nom'].'</option>';
        }
        $rep = $this->racineSite."Applications/";
        $dir = opendir($rep);
        while ($f = readdir($dir)) {
            if(is_dir($rep.$f) && $f != "." && $f != "..") {
                if("Applications/".$f == $donnees['lien']) {
                    $retour .= '<option value="Applications/'.$f.'" selected="selected">'.$f.'</option>';
                } else {
                    $retour .= '<option value="Applications/'.$f.'">'.$f.'</option>';
                }
            }
        }
        closedir($dir);

        $req2->closeCursor();
        $retour .= '</select>';
        $retour .= '<input type="submit" value = "Creer" /></div></form>';
        $retour .= '</li>';
        $retour .= '</ul>';
        $req->closeCursor();
        return $retour;
    }

    protected function listeActions() {
        $retour = array();
        $retour[] = 'Editer';
        return $retour;
    }

    protected function listeModules() {
        $retour = array();
        $retour[] = $this;
        return $retour;
    }
}

?>

<?php
header ('Content-type: text/css');


class Css {
    public static function getCSS() {
        require_once('../../../../Bdd/config.php');
        require_once('../../../../Bdd/Bdd.class.php');

        $req = Bdd::execute('SELECT * FROM style order by id', array());
        $retour = '';
        if($req->rowCount() > 0) {
            while ($donnees = $req->fetch()) {
                $retour .= html_entity_decode($donnees['content']);
            }
        }
        $req->closeCursor();
        return Css::compress($retour);
    }

    public static function compress($content) {
        // Suppression des commentaires
        $content = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $content);

        // Suppression des tabulations, espaces multiples, retours à la ligne, etc.
        $content = str_replace(array('\r\n', '\r', '\n'), '', $content);
        $content = str_replace('\t', '', $content);
        $content =  preg_replace('#\s{2,}#i', '', $content);

        // Suppression des derniers espaces inutiles
        $content = str_replace(array(' { ',' {','{ '), '{', $content);
        $content = str_replace(array(' } ',' }','} '), '}', $content);
        $content = str_replace(array(' : ',' :',': '), ':', $content);

        return $content;
    }
}

echo Css::getCSS();
?>

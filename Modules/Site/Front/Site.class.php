<?php

class Site extends FrontOffice {

    protected $header;
    protected $menu;
    protected $menuHorizontal;
    protected $page;
    protected $footer;
    protected $search;

    public function __construct($racineSite) {
        parent::init('Site', $racineSite);

        require_once($racineSite.'Modules/Search/Front/Search.class.php');
        $this->search = new Search($racineSite);

        require_once($racineSite.'Modules/Header/Front/Header.class.php');
        $this->header = new Header($racineSite, $this->search);

        require_once($racineSite.'Modules/Menu/Front/Menu.class.php');
        $this->menu = new Menu($racineSite);

        require_once($racineSite.'Modules/MenuHorizontal/Front/MenuHorizontal.class.php');
        $this->menuHorizontal = new MenuHorizontal($racineSite);

        require_once($racineSite.'Modules/Footer/Front/Footer.class.php');
        $this->footer = new Footer($racineSite);

        require_once($racineSite.'Modules/Page/Front/Page.class.php');
        $this->page = new Page($racineSite, $this->search);
    }

    public function action() {
        switch($this->action) {
            default: break;
        }
        $this->header->action();
        $this->menu->action();
        $this->menuHorizontal->action();
        $this->page->action();
        $this->footer->action();
        $this->search->action();
    }

    public function affiche() {
        $retour = '';
        switch($this->affiche) {
            default: $retour .= $this->afficheDefaut(); break;
        }
        return $retour;
    }

    protected function afficheDefaut() {
        $headerHTML = $this->headerHTML();
        $header = $this->header->affiche();
        $menu = $this->menu->affiche();
        $menuHorizontal = $this->menuHorizontal->affiche();
        $page = $this->page->affiche();
        $footer = $this->footer->affiche();

        $retour = $headerHTML.'<body>'.$header.$menu.$menuHorizontal.$page.$footer;

        foreach(array_unique($this->script(), SORT_STRING) as $fic) {
            $retour .= '<script type="text/javascript" src="'.$fic.'"></script>';
        }

        $req = Bdd::execute('SELECT * FROM scripts', array());
        $donnees = $req->fetch();
        $retour .= html_entity_decode($donnees['content']);

        $retour .= '</body></html>';

        $retour = preg_replace('#href="/([a-z0-9]+).(html|htm)"#i', 'href="$1-'.Utils::$GET['langue'].'.$2"', $retour);
        return $retour;
    }

    protected function headerHTML() {
        $retour = '';
        $retour .= '<!DOCTYPE html>';
        $retour .= '<html lang="'.Utils::$GET['langue'].'">';
        $retour .= '<head>';

        $retour .= '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">';
        $retour .= '<meta name="author" lang="fr" content="Tristan Kahn" />';
        $retour .= '<meta name="publisher" content="Tristan Kahn" />';
        $retour .= '<meta name="keywords" lang="'.Utils::$GET['langue'].'" content="'.$this->page->keywords().'" />';
        $retour .= '<meta name="google-site-verification" content="vVtSzxU_wereciLnVMB7MKBfafl6uuCPOdnRw7h9RvU" />';
        $retour .= '<meta name="Description" content="'.$this->page->description().'" />';
        $retour .= '<title>'.$this->page->title().'</title>';
        $retour .= '<link rel="shortcut icon" href="'.$this->racineSite.'favicon.ico" />';
        $retour .= '<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->';
        $req = Bdd::execute('SELECT * FROM metas', array());
        $donnees = $req->fetch();
        $retour .= html_entity_decode($donnees['content']);

        foreach(array_unique($this->css(), SORT_STRING) as $fic) {
            $retour .= '<link rel="stylesheet" type="text/css" media="screen" href="'.$fic.'" />';
        }

        $retour .= '</head>';
        return $retour;
    }

    protected function css() {
        $fichiers = array();
        $fichiers[] = $this->racineSite.'Modules/'.$this->nom.'/Front/Styles/styles.php';
        $fichiers[] = $this->racineSite.'Plugins/lightbox/lightbox.css';
        foreach($this->header->css() as $fic) {
            $fichiers[] = $fic;
        }
        foreach($this->menu->css() as $fic) {
            $fichiers[] = $fic;
        }
        foreach($this->menuHorizontal->css() as $fic) {
            $fichiers[] = $fic;
        }
        foreach($this->page->css() as $fic) {
            $fichiers[] = $fic;
        }
        foreach($this->footer->css() as $fic) {
            $fichiers[] = $fic;
        }
        return $fichiers;
    }

    protected function script() {
        $fichiers = array();
        $fichiers[] = $this->racineSite.'Plugins/jquery-1.10.2.min.js';
        $fichiers[] = $this->racineSite.'Plugins/lightbox/lightbox.js';
        foreach($this->header->script() as $fic) {
            $fichiers[] = $fic;
        }
        foreach($this->menu->script() as $fic) {
            $fichiers[] = $fic;
        }
        foreach($this->menuHorizontal->script() as $fic) {
            $fichiers[] = $fic;
        }
        foreach($this->page->script() as $fic) {
            $fichiers[] = $fic;
        }
        foreach($this->footer->script() as $fic) {
            $fichiers[] = $fic;
        }
        return $fichiers;
    }
}

?>

$(function() {
    $("#tabs").tabs();
    try {
        if (document.getElementById("content") != null) {
            $("#content").css("display", "none");
            CodeMirror.fromTextArea(document.getElementById("content"), {
                styleActiveLine: true,
                lineNumbers: true,
                lineWrapping: true,
                mode : "css"
            });
        }
    } catch(error) { }

    try {
        if (document.getElementById("contentJavascript") != null) {
            $("#contentJavascript").css("display", "none");
            CodeMirror.commands.autocomplete = function(cm) {
                CodeMirror.showHint(cm, CodeMirror.hint.javascript);
            }
            CodeMirror.fromTextArea(document.getElementById("contentJavascript"), {
                styleActiveLine: true,
                lineNumbers: true,
                lineWrapping: true,
                extraKeys: {"Ctrl-Space": "autocomplete"},
                mode: "text/javascript"
            });
        }
    } catch(error) { }

    try {
        if (document.getElementById("contentMetas") != null) {
            $("#contentMetas").css("display", "none");
            CodeMirror.commands.autocomplete = function(cm) {
                CodeMirror.showHint(cm, CodeMirror.hint.javascript);
            }
            CodeMirror.fromTextArea(document.getElementById("contentMetas"), {
                styleActiveLine: true,
                lineNumbers: true,
                lineWrapping: true,
                extraKeys: {"Ctrl-Space": "autocomplete"},
                mode: "text/javascript"
            });
        }
    } catch(error) { }
});

function openKCFinder(field) {
    var url = '/Plugins/kcfinder/browse.php?type=' + field + '&lang=fr';
    var caption = 'Serveur';
    var options = 'status=0, toolbar=0, location=0, menubar=0, directories=0, resizable=1, scrollbars=0, width=800, height=600';
    window.open(url, caption, options);
}

<?php

class Site extends BackOffice {

    protected $header;
    protected $menu;
    protected $menuHorizontal;
    protected $page;
    protected $footer;
    protected $derModif;

    public function __construct($racineSite) {
        parent::init('Site', $racineSite);

        require_once($racineSite.'Modules/Header/Back/Header.class.php');
        $this->header = new Header($racineSite);

        require_once($racineSite.'Modules/Menu/Back/Menu.class.php');
        $this->menu = new Menu($racineSite);

        require_once($racineSite.'Modules/MenuHorizontal/Back/MenuHorizontal.class.php');
        $this->menuHorizontal = new MenuHorizontal($racineSite);

        require_once($racineSite.'Modules/Page/Back/Page.class.php');
        $this->page = new Page($racineSite);

        require_once($racineSite.'Modules/Footer/Back/Footer.class.php');
        $this->footer = new Footer($racineSite);

        require_once($racineSite.'Modules/DerModif/Back/DerModif.class.php');
        $this->derModif = new DerModif($racineSite);
    }

    public function action() {
        switch($this->action) {
            case 'creer': $this->creerCSS(); break;
            case 'editer': $this->editerCSS(); break;
            case 'supprimer': $this->supprimerCSS(); break;
            case 'modifyScript': $this->modifyScript(); break;
            case 'modifyMetas': $this->modifyMetas(); break;
            default: break;
        }
        $this->header->action();
        $this->menu->action();
        $this->menuHorizontal->action();
        $this->page->action();
        $this->footer->action();
        $this->derModif->action();
    }

    protected function creerCSS() {
        $req = Bdd::execute('INSERT INTO style (`id`, `nom`, `content`) VALUES (NULL , "'.htmlentities(Utils::$POST['nomStyle']).'", "'.htmlentities(Utils::$POST['content']).'")', array());
        $req->closeCursor();
        $this->affiche = 'Lister-CSS';
    }

    protected function editerCSS() {
        $req = Bdd::execute('UPDATE style SET `nom` = "'.htmlentities(Utils::$POST['nomStyle']).'", `content` = "'.htmlentities(Utils::$POST['content']).'" WHERE `id` = '.htmlentities(Utils::$POST['idStyle']), array());
        $req->closeCursor();
        $this->affiche = 'Lister-CSS';
    }

    protected function modifyScript() {
        $req = Bdd::execute('UPDATE scripts SET `content` = "'.htmlentities(Utils::$POST['contentJavascript']).'"', array());
        $req->closeCursor();
        $this->affiche = 'Editer-Scripts';
    }

    protected function modifyMetas() {
        $req = Bdd::execute('UPDATE metas SET `content` = "'.htmlentities(Utils::$POST['contentMetas']).'"', array());
        $req->closeCursor();
        $this->affiche = 'Editer-Metas';
    }

    protected function supprimerCSS() {
        $req = Bdd::execute('DELETE FROM style WHERE id = '.htmlentities(Utils::$POST['idStyle']), array());
        $req->closeCursor();
        $this->affiche = 'Lister-CSS';
    }

    public function affiche() {
        $retour = $this->headerHTML();
        $retour .= '<body>';
        $retour .= $this->header();
        $retour .= $this->menu();

        $retour .= '<div id="bodySite">';
        switch($this->affiche) {
            case 'Lister-CSS': $retour .= $this->afficheListeCSS(); break;
            case 'Creer-CSS': $retour .= $this->afficheCreerCSS(); break;
            case 'Editer-CSS': $retour .= $this->afficheEditerCSS(); break;
            case 'Editer-Scripts': $retour .= $this->afficheEditerScripts(); break;
            case 'Editer-Metas': $retour .= $this->afficheEditerMetas(); break;
            default: $retour .= $this->afficheDefaut(); break;
        }
        $retour .= '</div>';
        $retour .= $this->footer();

        foreach(array_unique($this->script(), SORT_STRING) as $fic) {
            $retour .= '<script type="text/javascript" src="'.$fic.'"></script>';
        }

        $retour .= '</body>';
        $retour .= '</html>';
        return $retour;
    }

    protected function afficheDefaut() {
        $retour = '';
        if(isset(Utils::$POST['nom'])) {
            $retour .= '<div id="defaut"><h1>Modification du module : '.Utils::$POST['nom'].'</h1></div>';
            foreach($this->listeModules() as $mod) {
                if($mod->getNom() == htmlentities(Utils::$POST['nom'])) {
                    $retour .= $mod->affiche();
                }
            }
        }
        else {
            $retour .= '<div id="defaut"><h2>selectionnez une action à effectuer dans le menu</h2></div>';
        }

        return $retour;
    }

    protected function afficheListeCSS() {
        $retour = '<div id="defaut"><h1>Modification du module : Style</h1></div>';
        $retour .= '<div id="style">';
        $req = Bdd::execute('SELECT * FROM style order by id', array());
        $retour .= '<table id="listeStyle">';
        $retour .= '<tr>';
        $retour .= '<th>Nom du Style</th>';
        $retour .= '<th>Editer le Style</th>';
        $retour .= '<th>Supprimer le Style</th>';
        $retour .= '</tr>';

        if($req->rowCount() > 0) {
            while ($donnees = $req->fetch()) {
                $retour .= '<tr>';
                $retour .= '<td>'.$donnees['nom'].'</td>';
                // Edition
                $retour .= '<td><form action="" method="post">';
                $retour .= '<p><input type="hidden" name="nom" value="'.$this->getNom().'" />';
                $retour .= '<input type="hidden" name="affiche" value="Editer-CSS" />';
                $retour .= '<input type="hidden" name="idStyle" value="'.$donnees['id'].'" />';
                $retour .= '<input alt="Editer" src="'.$this->racineSite.'Images/editer.png" type="image" /></p></form></td>';
                // Suppression
                $retour .= '<td><form action="" method="post" onsubmit="return confirm(\'Etes-vous sur?\');">';
                $retour .= '<p><input type="hidden" name="nom" value="'.$this->getNom().'" />';
                $retour .= '<input type="hidden" name="action" value="supprimer" />';
                $retour .= '<input type="hidden" name="idStyle" value="'.$donnees['id'].'" />';
                $retour .= '<input alt="Supprimer" src="'.$this->racineSite.'Images/delete.png" type="image"/></p></form></td>';
                $retour .= '</tr>';
            }
        }
        $retour .= '</table>';
        $retour .= '</div>';

        $req->closeCursor();
        return $retour;
    }

    protected function afficheCreerCSS() {
        $retour = '<div id="defaut"><h1>Modification du module : Style</h1></div>';
        $retour .= '<div id="style">';
        $retour .= '<form action="" method="post">';
        $retour .= '<p><input type="hidden" name="nom" value="'.$this->getNom().'" />';
        $retour .= '<input type="hidden" name="action" value="creer" />';
        $retour .= '<label for="nomPage">Nom du Style : </label>';
        $retour .= '<input type="text" name="nomStyle" id="nomStyle" value="" size="40" maxlength="40" /></p>';
        $retour .= '<p><label for="content">Contenu du Style : </label></p>';
        $retour .= '<p><textarea id="content" name="content"></textarea></p>';
        $retour .= '<p><input alt="Valider" src="'.$this->racineSite.'Images/valider.png" type="image" /></p></form>';
        $retour .= '</div>';
        return $retour;
    }

    protected function afficheEditerCSS() {
        $retour = '<div id="defaut"><h1>Modification du module : Style</h1></div>';
        $retour .= '<div id="style">';
        $req = Bdd::execute('SELECT * FROM style WHERE id = '.htmlentities(Utils::$POST['idStyle']), array());
        $donnees = $req->fetch();

        $retour .= '<form action="" method="post">';
        $retour .= '<p><input type="hidden" name="nom" value="'.$this->getNom().'" />';
        $retour .= '<input type="hidden" name="idStyle" value="'.htmlentities(Utils::$POST['idStyle']).'" />';
        $retour .= '<input type="hidden" name="action" value="editer" />';
        $retour .= '<label for="nomPage">Nom du Style : </label>';
        $retour .= '<input type="text" name="nomStyle" id="nomStyle" value="'.$donnees['nom'].'" size="40" maxlength="40" /></p>';
        $retour .= '<p><label for="contenuPage">Contenu du Style : </label></p>';
        $retour .= '<p><textarea id="content" name="content">'.html_entity_decode($donnees['content']).'</textarea></p>';
        $retour .= '<p><input alt="Valider" src="'.$this->racineSite.'Images/valider.png" type="image" /></p></form>';
        $retour .= '</div>';

        return $retour;
    }

    protected function afficheEditerScripts() {
        $retour = '<div id="defaut"><h1>Modification du module : Scripts</h1></div>';
        $retour .= '<div id="scripts">';
        $req = Bdd::execute('SELECT * FROM scripts', array());
        $donnees = $req->fetch();

        $retour .= '<form action="" method="post">';
        $retour .= '<p><input type="hidden" name="nom" value="'.$this->getNom().'" />';
        $retour .= '<input type="hidden" name="action" value="modifyScript" />';
        $retour .= '<p><textarea id="contentJavascript" name="contentJavascript">'.html_entity_decode($donnees['content']).'</textarea></p>';
        $retour .= '<p><input alt="Valider" src="'.$this->racineSite.'Images/valider.png" type="image" /></p></form>';
        $retour .= '</div>';

        return $retour;
    }

    protected function afficheEditerMetas() {
        $retour = '<div id="defaut"><h1>Modification du module : Metas</h1></div>';
        $retour .= '<div id="metas">';
        $req = Bdd::execute('SELECT * FROM metas', array());
        $donnees = $req->fetch();

        $retour .= '<form action="" method="post">';
        $retour .= '<p><input type="hidden" name="nom" value="'.$this->getNom().'" />';
        $retour .= '<input type="hidden" name="action" value="modifyMetas" />';
        $retour .= '<p><textarea id="contentMetas" name="contentMetas">'.html_entity_decode($donnees['content']).'</textarea></p>';
        $retour .= '<p><input alt="Valider" src="'.$this->racineSite.'Images/valider.png" type="image" /></p></form>';
        $retour .= '</div>';

        return $retour;
    }

    protected function header() {
        $retour = '<div id="headerSite"><h1>Bienvenu sur le site d\'administration du site "haiecapique"</h1></div>';
        return $retour;
    }

    protected function menu() {
        $retour = '<div id="menuSite">';
        $retour .= '<table><tr>';
        $listeModules = $this->listeModules();
        foreach($listeModules as $mod) {
            $retour .= '<th class='.$mod->getNom().'>'.$mod->getNom().'</th>';
        }
        $retour .= '<th>Scripts</th>';
        $retour .= '<th>Serveur</th>';
        $retour .= '<th>Logout</th>';

        $retour .= '</tr><tr>';
        foreach($listeModules as $mod) {
            $retour .= '<td class='.$mod->getNom().'>';
            $acts = $mod->listeActions();
            if($acts) {
                foreach($acts as $act) {
                    $retour .= '<form action="" method="post">';
                    $retour .= '<input type="hidden" name="nom" value="'.$mod->getNom().'" />';
                    $retour .= '<input type="hidden" name="affiche" value="'.$act.'" />';
                    $retour .= '<input class="btn" type="submit" value="'.$act.'" />';
                    $retour .= '</form>';
                }
            }
            $retour .= '</td>';
        }

        $retour .= '<td><form action="" method="post">';
        $retour .= '<input type="hidden" name="nom" value="'.$this->getNom().'" />';
        $retour .= '<input type="hidden" name="affiche" value="Editer-Scripts" />';
        $retour .= '<input class="btn" type="submit" value="Editer Scripts" />';
        $retour .= '</form><form action="" method="post">';
        $retour .= '<input type="hidden" name="nom" value="'.$this->getNom().'" />';
        $retour .= '<input type="hidden" name="affiche" value="Editer-Metas" />';
        $retour .= '<input class="btn" type="submit" value="Editer Metas" />';
        $retour .= '</form></td>';

        $retour .= '<td><input class="btn" type="submit" onclick="openKCFinder(\'files\')" value="Files" />';
        $retour .= '<input class="btn" type="submit" onclick="openKCFinder(\'images\')" value="Images" />';
        $retour .= '<input class="btn" type="submit" onclick="openKCFinder(\'flash\')" value="Flash" /></td>';

        $retour .= '<td><form action="index.php" method="post">';
        $retour .= '<input type="hidden" name="logout" value="" />';
        $retour .= '<input class="btn" type="submit" value="Logout" /></form></td>';

        $retour .= '</tr></table>';
        $retour .= '</div>';

        return $retour;
    }

    protected function footer() {
        $retour = '<div id="footerSite">';
        $retour .= '</div>';
        return $retour;
    }

    protected function listeActions() {
        $retour = array();
        $retour[] = 'Lister-CSS';
        $retour[] = 'Creer-CSS';
        return $retour;
    }

    protected function listeModules() {
        $retour = array();
        foreach($this->header->listeModules() as $element) {
            $retour[] = $element;
        }
        foreach($this->menu->listeModules() as $element) {
            $retour[] = $element;
        }
        foreach($this->menuHorizontal->listeModules() as $element) {
            $retour[] = $element;
        }
        foreach($this->page->listeModules() as $element) {
            $retour[] = $element;
        }
        foreach($this->footer->listeModules() as $element) {
            $retour[] = $element;
        }
        foreach($this->derModif->listeModules() as $element) {
            $retour[] = $element;
        }
        $retour[] = $this;
        return $retour;
    }

    protected function headerHTML() {
        $retour = '';
        $retour .= '<!DOCTYPE html>';
        $retour .= '<html lang="fr">';
        $retour .= '<head>';
        $retour .= '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">';
        $retour .= '<meta name="author" lang="fr" content="Tristan Kahn" />';
        $retour .= '<meta name="publisher" content="Tristan Kahn" />';
        $retour .= '<meta name="keywords" lang="fr" content="Administration" />';
        $retour .= '<meta name="Description" content="Administration" />';
        $retour .= '<title>Administration - haiecapique</title>';
        $retour .= '<link rel="shortcut icon" href="'.$this->racineSite.'favicon.ico" />';
        $retour .= '<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->';

        foreach(array_unique($this->css(), SORT_STRING) as $fic) {
            $retour .= '<link rel="stylesheet" type="text/css" media="screen" href="'.$fic.'" />';
        }

        $retour .= '</head>';
        return $retour;
    }

    protected function css() {
        $fichiers = array();

        foreach($this->header->css() as $fic) {
            $fichiers[] = $fic;
        }
        foreach($this->menu->css() as $fic) {
            $fichiers[] = $fic;
        }
        foreach($this->menuHorizontal->css() as $fic) {
            $fichiers[] = $fic;
        }
        foreach($this->page->css() as $fic) {
            $fichiers[] = $fic;
        }
        foreach($this->footer->css() as $fic) {
            $fichiers[] = $fic;
        }
        foreach($this->derModif->css() as $fic) {
            $fichiers[] = $fic;
        }
        $fichiers[] = $this->racineSite.'Plugins/JQueryUI/css/smoothness/jquery-ui-1.10.3.custom.min.css';
        $fichiers[] = $this->racineSite.'Plugins/codemirror-3.15/codemirror.css';
        $fichiers[] = $this->racineSite.'Plugins/codemirror-3.15/show-hint.css';
        $fichiers[] = $this->racineSite.'Modules/'.$this->nom.'/Back/Styles/styles.css';
        return $fichiers;
    }

    protected function script() {
        $fichiers = array();

        $fichiers[] = $this->racineSite.'Plugins/jquery-1.10.2.min.js';
        $fichiers[] = $this->racineSite.'Plugins/JQueryUI/js/jquery-ui-1.10.3.custom.min.js';
        $fichiers[] = $this->racineSite.'Plugins/ckeditor/ckeditor.js';
        $fichiers[] = $this->racineSite.'Plugins/codemirror-3.15/codemirror.js';
        $fichiers[] = $this->racineSite.'Plugins/codemirror-3.15/css.js';
        $fichiers[] = $this->racineSite.'Plugins/codemirror-3.15/javascript.js';
        $fichiers[] = $this->racineSite.'Plugins/codemirror-3.15/show-hint.js';
        $fichiers[] = $this->racineSite.'Plugins/codemirror-3.15/javascript-hint.js';

        $fichiers[] = $this->racineSite.'Modules/'.$this->nom.'/Back/Scripts/script.js';

        foreach($this->header->script() as $fic) {
            $fichiers[] = $fic;
        }
        foreach($this->menu->script() as $fic) {
            $fichiers[] = $fic;
        }
        foreach($this->menuHorizontal->script() as $fic) {
            $fichiers[] = $fic;
        }
        foreach($this->page->script() as $fic) {
            $fichiers[] = $fic;
        }
        foreach($this->footer->script() as $fic) {
            $fichiers[] = $fic;
        }
        foreach($this->derModif->script() as $fic) {
            $fichiers[] = $fic;
        }
        return $fichiers;
    }
}

?>

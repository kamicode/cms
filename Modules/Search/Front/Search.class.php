<?php

class Search extends FrontOffice {

    protected $result;

    public function __construct($racineSite) {
        parent::init('Search', $racineSite);
        
        $this->result = array();
    }

    public function action() {
        switch($this->action) {
            case 'search': $this->search(); break;
            default: break;
        }
    }

    public function affiche() {
        $retour = '<div id="search">';
        switch($this->affiche) {
            //case 'sended': $retour .= $this->sended(); break;
            default: $retour .= $this->afficheDefaut(); break;
        }
        $retour .= '</div>';
        return $retour;
    }

    protected function afficheDefautEN() {
        $retour = "";
        $retour .= '<form action="search-fr.html" method="post">';
        $retour .= '<input type="hidden" name="action" value="search" />';
        $retour .= '<input type="hidden" name="nom" value="'.$this->getNom().'" />';
        $retour .= '<p>Entrez un mot clé:';
        $retour .= '<input type="text" name="Mot" size="15">';
        $retour .= '<input type="submit" value="Rechercher" alt="Lancer la recherche!"></p>';
        $retour .= '</form>';
        return $retour;
    }
    protected function afficheDefautFR() {
        $retour = "";
        $retour .= '<form action="search-fr.html" method="post">';
        $retour .= '<input type="hidden" name="action" value="search" />';
        $retour .= '<input type="hidden" name="nom" value="'.$this->getNom().'" />';
        $retour .= '<p>Entrez un mot clé:';
        $retour .= '<input type="text" name="Mot" size="15">';
        $retour .= '<input type="submit" value="Rechercher" alt="Lancer la recherche!"></p>';
        $retour .= '</form>';
        return $retour;
    }

    protected function afficheDefaut() {
        if(Utils::$GET['langue'] == 'fr') {
            return $this->afficheDefautFR();
        }
        else {
            return $this->afficheDefautEN();
        }
    }
    
    public function afficheResult() {
        $retour = "";
        foreach($this->result as $res) {
            $retour .= '<a href="'.$res.'-fr.html">'.$res.'</a><br />';
        }
        return $retour;
    }

    protected function search() {
        $req = Bdd::execute('SELECT distinct `nom` FROM page WHERE `contenu-fr` LIKE "%'.html_entity_decode(Utils::$POST['Mot']).'%" ORDER BY `title-fr` ASC', array());
        if($req->rowCount() > 0) {     
            while ($donnees = $req->fetch()) {
                $this->result[] = $donnees['nom'];
            }
        }
    }
}

?>

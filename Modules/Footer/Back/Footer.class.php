<?php

class Footer extends BackOffice {

    public function __construct($racineSite) {
        parent::init('Footer', $racineSite);
    }

    public function action() {
        switch($this->action) {
            case 'editer': $this->editer(); break;
            default: break;
        }
    }

    protected function editer() {
        Bdd::execute('UPDATE footer SET contenu-fr = ?, contenu-en = ?', array(htmlentities(Utils::$POST['contenuFooter']), htmlentities(Utils::$POST['traductionFooter'])), array());
        $req->closeCursor();
        $this->affiche = 'Editer';
    }

    public function affiche() {
        $retour = '<div id="footerBack">';
        switch($this->affiche) {
            case 'Editer': $retour .= $this->afficheEditer(); break;
            default: $retour .= $this->afficheDefaut(); break;
        }
        $retour .= '</div>';
        return $retour;
    }

    protected function afficheDefaut() {
        return 'affichage par defaut du module '.$this->nom.' coté admin';
    }

    protected function afficheEditer() {
        $req = Bdd::execute('SELECT * FROM footer', array());
        $donnees = $req->fetch();

        $retour = '<form action="" method="post">';
        $retour .= '<p><input type="hidden" name="nom" value="'.$this->getNom().'" />';
        $retour .= '<input type="hidden" name="action" value="editer" />';

        $retour .= '<div id="tabs">';
        $retour .= '<ul>';
        $retour .= '<li><a href="#tabs-fr">Français</a></li>';
        $retour .= '<li><a href="#tabs-en">Anglais</a></li>';
        $retour .= '</ul>';

        $retour .= '<div id="tabs-fr">';
        $retour .= '<p><textarea cols="80" name="contenuFooter" id="contenuFooter" rows="10">'.html_entity_decode($donnees['contenu-fr']).'</textarea></p>';
        $retour .= '</div>';

        $retour .= '<div id="tabs-en">';
        $retour .= '<p><textarea cols="80" name="traductionFooter" id="traductionFooter" rows="10">'.html_entity_decode($donnees['contenu-en']).'</textarea></p>';
        $retour .= '</div>';
        $retour .= '</div>';

        $retour .= '<p><input alt="Valider" src="'.$this->racineSite.'Images/valider.png" type="image" /></p></form>';
        return $retour;
    }

    protected function listeActions() {
        $retour = array();
        $retour[] = 'Editer';
        return $retour;
    }

    protected function listeModules() {
        $retour = array();
        $retour[] = $this;
        return $retour;
    }

    protected function script() {
        $fichiers = array();
        $fichiers[] = $this->racineSite.'Modules/'.$this->nom.'/Back/Scripts/script.js';
        return $fichiers;
    }
}

?>

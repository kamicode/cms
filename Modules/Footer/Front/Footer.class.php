<?php

class Footer extends FrontOffice {

    public function __construct($racineSite) {
        parent::init('Footer', $racineSite);
    }

    public function action() {
        switch($this->action) {
            default: break;
        }
    }

    public function affiche() {
        $retour = '<div id="footer">';
        switch($this->affiche) {
            default: $retour .= $this->afficheDefaut(); break;
        }
        $retour .= '</div>';
        return $retour;
    }

    protected function afficheDefaut() {
        $req = Bdd::execute('SELECT * FROM footer', array());
        $donnees = $req->fetch();
        $req->closeCursor();
        return html_entity_decode($donnees['contenu-'.Utils::$GET['langue']]);
    }
}

?>

<?php

class DerModif extends FrontOffice {

    public function __construct($racineSite) {
        parent::init('DerModif', $racineSite);
    }

    public function action() {
        switch($this->action) {
            default: break;
        }
    }

    public function affiche() {
        $retour = '<div id="derModif">';
        switch($this->affiche) {
            default: $retour .= $this->afficheDefaut(); break;
        }
        $retour .= '</div>';
        return $retour;
    }

    protected function afficheDefaut() {
        $retour = "";
        $req = Bdd::execute('SELECT * FROM dermodif', array());
        while ($donnees = $req->fetch()) {
            if(Utils::$GET['langue'] == 'fr') {
                $retour .= 'Dernière mise à jour du site web :<br />'.$donnees['date'];
            }
            else if(Utils::$GET['langue'] == 'en') {
                $retour .= 'Last update of website :<br />'.$donnees['date'];
            }
        }
        return $retour;
    }
}

?>

<?php

class DerModif extends BackOffice {

    public function __construct($racineSite) {
        parent::init('DerModif', $racineSite);
    }

    public function action() {
        switch($this->action) {
            default: break;
        }
        if(isset(Utils::$POST['action']) && Utils::$POST['action'] != '') {
            $req = Bdd::execute('UPDATE dermodif SET date = ?', array(date('d-m-Y')));
            $req->closeCursor();
            //We need to send mail ? $this->sendMail();
            $this->saveBDD();
        }
    }

    public function sendMail() {
        $headers = 'From: "Haiecapique"<contact@haiecapique.fr>'."\r\n";
        $headers .= 'Reply-To: contact@haiecapique.fr'."\r\n";
        $headers .= 'Content-Type: text/html; charset="utf-8"'."\r\n";
        $headers .= 'Content-Transfer-Encoding: 8bit'."\r\n";
        $to = 'contact@haiecapique.fr';
        $sujet = 'Modification du site "Haiecapique"';
        $message = 'Le site "Haiecapique" a subit une modification :<br /><br />';

        $message .= 'User : '.Utils::$SESSION['login'].'<br />';

        if($_SERVER['REMOTE_USER'] != "")
            $message .= 'REMOTE_USER : '.$_SERVER['REMOTE_USER'].'<br />';

        foreach(Utils::$GET as $key => $val)
            $message .= 'Utils::$GET["'.$key.'"]='.htmlentities($val).'<br />';

        foreach(Utils::$POST as $key => $val) {
            if($key != 'contenuPage' && $key != 'traductionPage')
                $message .= 'Utils::$POST["'.$key.'"]='.htmlentities($val).'<br />';
        }

        if(Utils::$POST["contenuPage"] != "")
            $message .= 'Utils::$POST["contenuPage"]='.htmlentities(Utils::$POST["contenuPage"]).'<br />';
        if(Utils::$POST["traductionPage"] != "")
            $message .= 'Utils::$POST["traductionPage"]='.htmlentities(Utils::$POST["traductionPage"]).'<br />';

        mail($to, $sujet, $message, $headers);
    }

    public function saveBDD() {
        $path = getenv('DOCUMENT_ROOT')."/SaveBDD";
        $file = 'Save-'.date('Y-m-d').'.sql.gz';
        if(!file_exists($path."/".$file)) {
            $host = DBASE_SERVER;
            $user = DBASE_LOGIN;
            $pass = DBASE_PASS;
            $db = DBASE_BASENAME;

            system(sprintf('mysqldump --opt -h%s -u%s -p"%s" %s | gzip > %s/%s', $host, $user, $pass, $db, $path, $file));
        }
    }

    public function affiche() {
        $retour = '<div id="derModifBack">';
        switch($this->affiche) {
            default: $retour .= $this->afficheDefaut(); break;
        }
        $retour .= '</div>';
        return $retour;
    }

    protected function afficheDefaut() {
        $retour = 'affichage par defaut du module '.$this->nom.' coté admin';
        return $retour;
    }

    protected function listeActions() {
        $retour = array();
        return $retour;
    }

    protected function listeModules() {
        $retour = array();
        return $retour;
    }
}

?>

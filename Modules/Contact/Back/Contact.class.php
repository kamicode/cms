<?php

class Contact extends BackOffice {

    public function __construct($racineSite) {
        parent::init('Page', $racineSite);
    }

    public function action() {
        switch($this->action) {
            default: break;
        }
    }

    public function affiche() {
        $retour = '<div id="contactBack">';
        switch($this->affiche) {
            default: $retour .= $this->afficheDefaut(); break;
        }
        $retour .= '</div>';
        return $retour;
    }

    protected function afficheDefaut() {
        return 'affichage par defaut du module '.$this->nom.' coté admin';
    }

    protected function listeActions() {
        $retour = array();
        return $retour;
    }

    protected function listeModules() {
        $retour = array();
        $retour[] = $this;
        return $retour;
    }
}

?>

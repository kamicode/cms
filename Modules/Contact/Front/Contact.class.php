<?php

class Contact extends FrontOffice {

    public function __construct($racineSite) {
        parent::init('Contact', $racineSite);
    }

    public function action() {
        switch($this->action) {
            case 'send': $this->send(); break;
            default: break;
        }
    }

    public function affiche() {
        $retour = '<div id="contact">';
        switch($this->affiche) {
            case 'sended': $retour .= $this->sended(); break;
            default: $retour .= $this->afficheDefaut(); break;
        }
        $retour .= '</div>';
        return $retour;
    }

    protected function afficheDefautEN() {
        $retour = "";
        $retour .= '<form action="" method="post">';
        $retour .= '<input type="hidden" name="action" value="send" />';
        $retour .= '<input type="hidden" name="nom" value="'.$this->getNom().'" />';
        $retour .= '<p><label for="nomContact">First Name *: </label><br /><input type="text" name="nomContact" id="nomContact" value="" size="60" maxlength="60" required /></p>';
        $retour .= '<p><label for="prenomContact">Last Name *: </label><br /><input type="text" name="prenomContact" id="prenomContact" value="" size="60" maxlength="60" required /></p>';
        $retour .= '<p><label for="villeContact">Country : </label><br /><input type="text" name="villeContact" id="villeContact" value="" size="60" maxlength="60" /></p>';
        $retour .= '<p><label for="numContact">Call Number : </label><br /><input type="tel" name="numContact" id="numContact" value="" size="60" maxlength="60" /></p>';
        $retour .= '<p><label for="mailContact">Mail *: </label><br /><input type="email" name="mailContact" id="mailContact" value="" size="60" maxlength="60" required /></p>';
        $retour .= '<p><label for="diversContact">Message *: </label><br /><textarea name="diversContact" id="diversContact" rows="10" cols="60" style="width: 380px; height: 100px;" required></textarea></p>';
        $retour .= '<p><input alt="Valider" src="'.$this->racineSite.'Images/valider.png" type="image" /></p>';
        $retour .= '</form>';
        return $retour;
    }
    protected function afficheDefautFR() {
        $retour = "";
        $retour .= '<form action="" method="post">';
        $retour .= '<input type="hidden" name="action" value="send" />';
        $retour .= '<input type="hidden" name="nom" value="'.$this->getNom().'" />';
        $retour .= '<p><label for="nomContact">Nom *: </label><br /><input type="text" name="nomContact" id="nomContact" value="" size="60" maxlength="60" required /></p>';
        $retour .= '<p><label for="prenomContact">Prenom *: </label><br /><input type="text" name="prenomContact" id="prenomContact" value="" size="60" maxlength="60" required /></p>';
        $retour .= '<p><label for="villeContact">Ville : </label><br /><input type="text" name="villeContact" id="villeContact" value="" size="60" maxlength="60" /></p>';
        $retour .= '<p><label for="numContact">N° de téléphone : </label><br /><input type="tel" name="numContact" id="numContact" value="" size="60" maxlength="60" /></p>';
        $retour .= '<p><label for="mailContact">Mail *: </label><br /><input type="email" name="mailContact" id="mailContact" value="" size="60" maxlength="60" required /></p>';
        $retour .= '<p><label for="diversContact">Message *: </label><br /><textarea name="diversContact" id="diversContact" rows="10" cols="60" style="width: 376px; height: 100px;" required></textarea><br /></p>';
        $retour .= '<p><input alt="Valider" src="'.$this->racineSite.'Images/valider.png" type="image" /></p>';
        $retour .= '</form>';
        return $retour;
    }

    protected function afficheDefaut() {
        if(Utils::$GET['langue'] == 'fr') {
            return $this->afficheDefautFR();
        }
        else {
            return $this->afficheDefautEN();
        }
    }

    protected function send() {
        $headers ='From: "Haiecapique"<contact@haiecapique.fr>'."\r\n";
        $headers .='Reply-To: contact@haiecapique.fr'."\r\n";
        $headers .='Content-Type: text/html; charset="utf-8"'."\r\n";
        $headers .='Content-Transfer-Encoding: 8bit'."\r\n";
        $to = 'contact@haiecapique.fr';
        $sujet = 'Demande de contact via le site "Haiecapique"';

        $message = "";
        $message .= 'Vous avez une demande de contact via le site "Haiecapique" :<br />';
        $message .= 'Nom : '.html_entity_decode(Utils::$POST['nomContact']).'<br />';
        $message .= 'Prenom : '.html_entity_decode(Utils::$POST['prenomContact']).'<br />';
        $message .= 'Ville : '.html_entity_decode(Utils::$POST['villeContact']).'<br />';
        $message .= 'Mail : '.html_entity_decode(Utils::$POST['mailContact']).'<br />';
        $message .= 'Numéro : '.html_entity_decode(Utils::$POST['numContact']).'<br />';

        $divers = nl2br(stripslashes(html_entity_decode(Utils::$POST['diversContact'])));
        $message .= '<br />Message : <br />'.$divers.'<br />';

        mail($to, $sujet, $message, $headers);

        $this->affiche = 'sended';
    }

    protected function sended() {
        $retour = "";
        if(Utils::$GET['langue'] == 'fr') {
            $retour .= "Demande de contact envoyée";
        }
        else if(Utils::$GET['langue'] == 'en') {
            $retour .= "Contact request sent";
        }
        return $retour;
    }
}

?>

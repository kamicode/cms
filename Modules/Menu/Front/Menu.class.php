<?php

class Menu extends FrontOffice {

    protected $derModif;
    protected $index;

    public function __construct($racineSite) {
        parent::init('Menu', $racineSite);

        require_once($racineSite.'Modules/DerModif/Front/DerModif.class.php');
        $this->derModif = new DerModif($racineSite);
        $this->index = 0;
    }

    public function action() {
        switch($this->action) {
            default: break;
        }
    }

    public function affiche() {
        $retour = '<div id="menu">';
        switch($this->affiche) {
            default: $retour .= $this->afficheDefaut(); break;
        }
        $retour .= '</div>';
        return $retour;
    }

    protected function afficheDefaut() {
        $retour = $this->derModif->affiche();

        $retour .= '<div id="divMenu">';
        $req = Bdd::execute('SELECT * FROM menu WHERE idParent = 0 ORDER BY ordre ASC', array());

        if($req->rowCount() > 0) {
            while ($donnees = $req->fetch()) {
                $this->index = $this->index + 1;
                $retour .= '<div id="menu-'.$this->index.'">';
                if($donnees['lien'] != '') {
                    $retour .= '<a href="/'.$donnees['lien'].'.html">'.$donnees['nom'.'-'.Utils::$GET['langue']].'</a>';
                }
                else {
                    $retour .= '<h2>'.$donnees['nom'.'-'.Utils::$GET['langue']].'</h2>';
                }
                $retour .= $this->sousMenu($donnees['id']);
                $retour .= '</div>';
            }
        }
        $retour .= '</div>';

        $req->closeCursor();
        return $retour;
    }

    protected function sousMenu($idParent) {
        $retour = "";
        $req = Bdd::execute('SELECT * FROM menu WHERE idParent = ? ORDER BY ordre ASC', array($idParent));

        if($req->rowCount() > 0) {
            while ($donnees = $req->fetch()) {
                $this->index = $this->index + 1;
                $retour .= '<div id="menu-'.$this->index.'">';
                if($donnees['lien'] != '') {
                    $retour .= '<a href="/'.$donnees['lien'].'.html">'.$donnees['nom'.'-'.Utils::$GET['langue']].'</a>';
                }
                else {
                    $retour .= '<h2>'.$donnees['nom'.'-'.Utils::$GET['langue']].'</h2>';
                }
                $retour .= $this->sousMenu($donnees['id']);
                $retour .= '</div>';
            }
        }

        $req->closeCursor();
        return $retour;
    }

    protected function css() {
        $fichiers = array();
        foreach(parent::css() as $fic) {
            $fichiers[] = $fic;
        }
        foreach($this->derModif->css() as $fic) {
            $fichiers[] = $fic;
        }
        return $fichiers;
    }

    protected function script() {
        $fichiers = array();
        foreach(parent::script() as $fic) {
            $fichiers[] = $fic;
        }
        foreach($this->derModif->script() as $fic) {
            $fichiers[] = $fic;
        }
        return $fichiers;
    }
}

?>

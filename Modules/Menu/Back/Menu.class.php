<?php

class Menu extends BackOffice {

    public function __construct($racineSite) {
        parent::init('Menu', $racineSite);
    }

    public function action() {
        switch($this->action) {
            case 'editer': $this->editer(); break;
            case 'supprimer': $this->supprimer(htmlentities(Utils::$POST['idMenu'])); break;
            case 'creer': $this->creer(); break;
            case 'descendre': $this->descendre(); break;
            case 'monter': $this->monter(); break;
            default: break;
        }
    }

    protected function descendre() {
        $req = Bdd::execute('UPDATE menu SET `ordre` = "'.htmlentities(Utils::$POST['ordreMenu']).'" WHERE idParent = '.htmlentities(Utils::$POST['idParentMenu']).' AND ordre='.(htmlentities(Utils::$POST['ordreMenu']) + 1), array());
        $req->closeCursor();
        $req = Bdd::execute('UPDATE menu SET `ordre` = "'.(htmlentities(Utils::$POST['ordreMenu']) + 1).'" WHERE id = '.htmlentities(Utils::$POST['idMenu']), array());
        $req->closeCursor();
    }

    protected function monter() {
        $req = Bdd::execute('UPDATE menu SET `ordre` = "'.htmlentities(Utils::$POST['ordreMenu']).'" WHERE idParent = '.htmlentities(Utils::$POST['idParentMenu']).' AND ordre='.(htmlentities(Utils::$POST['ordreMenu']) - 1), array());
        $req->closeCursor();
        $req = Bdd::execute('UPDATE menu SET `ordre` = "'.(htmlentities(Utils::$POST['ordreMenu']) - 1).'" WHERE id = '.htmlentities(Utils::$POST['idMenu']), array());
        $req->closeCursor();
    }

    protected function creer() {
        $req = Bdd::execute('INSERT INTO menu (`id` , `idParent` , `nom-fr`, `nom-en`, `lien`, `ordre`) VALUES (NULL , "'.htmlentities(Utils::$POST['idParent']).'", "'.htmlentities(Utils::$POST['nomMenuFR']).'", "'.htmlentities(Utils::$POST['nomMenuEN']).'", "'.htmlentities(Utils::$POST['lienMenu']).'", "'.htmlentities(Utils::$POST['ordreMenu']).'")', array());
        $req->closeCursor();
        $this->affiche = 'liste';
    }

    protected function editer() {
        $req = Bdd::execute('UPDATE menu SET `nom-fr` = "'.htmlentities(Utils::$POST['nomMenuFR']).'", `nom-en` = "'.htmlentities(Utils::$POST['nomMenuEN']).'", lien = "'.htmlentities(Utils::$POST['lienMenu']).'" WHERE id = '.htmlentities(Utils::$POST['idMenu']), array());
        $req->closeCursor();
    }

    protected function supprimer($id) {
        // On supprime tous nos fils
        $req = Bdd::execute('SELECT * FROM menu WHERE `idParent` = ? ORDER BY ordre', array($id));
        while ($donnees = $req->fetch()) {
            $this->supprimer($donnees['id']);
        }
        $req->closeCursor();

        // On selection mon papa et mon ordre
        $req = Bdd::execute('SELECT idParent, ordre FROM `menu` WHERE `id` = ? ORDER BY ordre', array($id));
        $donnees = $req->fetch();
        $idParent = $donnees['idParent'];
        $ordre = $donnees['ordre'];
        $req->closeCursor();

        // On modifie l'ordre de tous nos petit freres
        $req = Bdd::execute('SELECT id FROM menu WHERE idParent = ? AND ordre > ? ORDER BY ordre', array($idParent, $ordre));
        while ($donnees = $req->fetch()) {
            $req2 = Bdd::execute('UPDATE menu SET ordre = ? WHERE id = ?', array($ordre, $donnees['id']));
            $req2->closeCursor();
            $ordre++;
        }
        $req->closeCursor();

        // On se supprime
        $req = Bdd::execute('DELETE FROM menu WHERE id = ?', array($id));
        $req->closeCursor();
    }

    public function affiche() {
        $retour = '<div id="menuBack">';
        switch($this->affiche) {
            default: $retour .= $this->afficheDefaut(); break;
        }
        $retour .= '</div>';
        return $retour;
    }

    protected function afficheDefaut() {
        $retour = '<ul>';
        $retour .= $this->menuGen(0, 15, 0, 1);
        $retour .= '</ul>';
        return $retour;
    }

    protected function menuGen($idParent, $nbFilsMax, $niveau, $nbNiveauMax) {
        $retour = "";
        $req = Bdd::execute('SELECT * FROM menu WHERE idParent = ? ORDER BY ordre', array($idParent));

        $nb_item = $req->rowCount();
        $i = 0;
        while ($donnees = $req->fetch()) {
            $i++;
            // Edition
            $retour .= '<li>';
            $retour .= '<form action="" method="post">';
            $retour .= '<div><input type="hidden" name="nom" value="'.$this->getNom().'" />';
            $retour .= '<input type="hidden" name="action" value="editer" />';
            $retour .= '<input type="hidden" name="idMenu" value="'.$donnees['id'].'" />';
            $retour .= '<label for="nomMenuFR'.$i.'">nom : </label><input type="text" name="nomMenuFR" id="nomMenuFR'.$i.'" value="'.$donnees['nom-fr'].'" size="30" maxlength="30" />';
            $retour .= '<label for="nomMenuEN'.$i.'"> traduction : </label><input type="text" name="nomMenuEN" id="nomMenuEN'.$i.'" value="'.$donnees['nom-en'].'" size="30" maxlength="30" />';
            $retour .= '<label for="lienMenu'.$i.'"> lien :</label><select name="lienMenu" id="lienMenu'.$i.'" style="width:100px">';
            $retour .= '<option value=""></option>';
            $req2 = Bdd::execute('SELECT nom FROM page ORDER BY nom', array());
            while ($donnees2 = $req2->fetch()) {
                if($donnees2['nom'] == $donnees['lien']) {
                    $retour .= '<option value="'.$donnees2['nom'].'" selected="selected">'.$donnees2['nom'].'</option>';
                }
                else {
                    $retour .= '<option value="'.$donnees2['nom'].'">'.$donnees2['nom'].'</option>';
                }
            }
            $rep = $this->racineSite."Applications/";
            $dir = opendir($rep);
            while ($f = readdir($dir)) {
                if(is_dir($rep.$f) && $f != "." && $f != "..") {
                    if("Applications/".$f == $donnees['lien']) {
                        $retour .= '<option value="Applications/'.$f.'" selected="selected">'.$f.'</option>';
                    } else {
                        $retour .= '<option value="Applications/'.$f.'">'.$f.'</option>';
                    }
                }
            }
            closedir($dir);

            $req2->closeCursor();
            $retour .= '</select>';
            $retour .= '<input type="submit" value="Editer" /></div></form>';

            // Ordre
            if($nb_item != 0) {
                if($donnees['ordre'] != $nb_item) {
                    $retour .= '<form action="" method="post">';
                    $retour .= '<div><input type="hidden" name="nom" value="'.$this->getNom().'" />';
                    $retour .= '<input type="hidden" name="action" value="descendre" />';
                    $retour .= '<input type="hidden" name="idMenu" value="'.$donnees['id'].'" />';
                    $retour .= '<input type="hidden" name="ordreMenu" value="'.$donnees['ordre'].'" />';
                    $retour .= '<input type="hidden" name="idParentMenu" value="'.$donnees['idParent'].'" />';
                    $retour .= '<input alt="" src="'.$this->racineSite.'Images/down.png" type="image" /></div></form>';
                }
                if($donnees['ordre'] != 1) {
                    $retour .= '<form action="" method="post">';
                    $retour .= '<div><input type="hidden" name="nom" value="'.$this->getNom().'" />';
                    $retour .= '<input type="hidden" name="action" value="monter" />';
                    $retour .= '<input type="hidden" name="idMenu" value="'.$donnees['id'].'" />';
                    $retour .= '<input type="hidden" name="ordreMenu" value="'.$donnees['ordre'].'" />';
                    $retour .= '<input type="hidden" name="idParentMenu" value="'.$donnees['idParent'].'" />';
                    $retour .= '<input alt="" src="'.$this->racineSite.'Images/up.png" type="image" /></div></form>';
                }
            }

            // Suppression
            $retour .= '<form action="" method="post" onsubmit="return confirm(\'Etes-vous sur?\');">';
            $retour .= '<div><input type="hidden" name="nom" value="'.$this->getNom().'" />';
            $retour .= '<input type="hidden" name="action" value="supprimer" />';
            $retour .= '<input type="hidden" name="idMenu" value="'.$donnees['id'].'" />';
            $retour .= '<input type="submit" value="Supprimer" /></div></form>';

            $retour .= '<ul>';
            // Fils
            if($niveau < $nbNiveauMax) {
                $retour .= $this->menuGen($donnees['id'], $nbFilsMax, $niveau + 1, $nbNiveauMax);
            }

            // Creation d'un nouveau fils
            $buf = 1;
            $req2 = Bdd::execute('SELECT id FROM menu WHERE idParent = '.$donnees['id'].' ORDER BY ordre', array());
            while ($donnees2 = $req2->fetch()) {
                $buf++;
            }
            $req2->closeCursor();
            if($niveau < $nbNiveauMax && $buf - 1 < $nbFilsMax) {
                $retour .= '<li>';

                $retour .= '<form action="" method="post">';
                $retour .= '<div><input type="hidden" name="nom" value="'.$this->getNom().'" />';
                $retour .= '<input type="hidden" name="action" value="creer" />';
                $retour .= '<input type="hidden" name="idParent" value="'.$donnees['id'].'" />';
                $retour .= '<input type="hidden" name="ordreMenu" value="'.$buf.'" />';
                $retour .= '<label for="nomMenuCreateFR'.$i.'">nom : </label><input type="text" name="nomMenuFR" id="nomMenuCreateFR'.$i.'" value="" size="30" maxlength="30" />';
                $retour .= '<label for="nomMenuCreateEN'.$i.'"> traduction : </label><input type="text" name="nomMenuEN" id="nomMenuCreateEN'.$i.'" value="" size="30" maxlength="30" />';
                $retour .= '<label for="lienMenuCreate'.$i.'"> lien :</label><select name="lienMenu" id="lienMenuCreate'.$i.'" style="width:100px">';
                $retour .= '<option value=""></option>';
                $req2 = Bdd::execute('SELECT nom FROM page ORDER BY nom', array());
                while ($donnees2 = $req2->fetch()) {
                    $retour .= '<option value="'.$donnees2['nom'].'">'.$donnees2['nom'].'</option>';
                }
                $rep = $this->racineSite."Applications/";
                $dir = opendir($rep);
                while ($f = readdir($dir)) {
                    if(is_dir($rep.$f) && $f != "." && $f != "..") {
                        if("Applications/".$f == $donnees['lien']) {
                            $retour .= '<option value="Applications/'.$f.'" selected="selected">'.$f.'</option>';
                        } else {
                            $retour .= '<option value="Applications/'.$f.'">'.$f.'</option>';
                        }
                    }
                }
                closedir($dir);

                $req2->closeCursor();
                $retour .= '</select>';
                $retour .= '<input type="submit" value = "Creer" /></div></form>';
                $retour .= '</li>';
            }
            $retour .= '</ul></li>';
        }

        // Creation Base
        if($idParent == 0) {
            if($i < $nbFilsMax) {
                $retour .= '<li>';
                $retour .= '<form action="" method="post">';
                $retour .= '<div><input type="hidden" name="nom" value="'.$this->getNom().'" />';
                $retour .= '<input type="hidden" name="action" value="creer" />';
                $retour .= '<input type="hidden" name="idParent" value="'.$idParent.'" />';
                $buf = 1;
                $req2 = Bdd::execute('SELECT ordre FROM menu WHERE idParent = ?', array($idParent));
                while ($donnees2 = $req2->fetch()) {
                    $buf++;
                }
                $req2->closeCursor();
                $retour .= '<input type="hidden" name="ordreMenu" value="'.$buf.'" />';
                $retour .= '<label for="nomMenuFRCreateB'.$i.'">nom : </label><input type="text" name="nomMenuFR" id="nomMenuFRCreateB'.$i.'" value="" size="30" maxlength="30" />';
                $retour .= '<label for="nomMenuENCreateB'.$i.'"> traduction : </label><input type="text" name="nomMenuEN" id="nomMenuENCreateB'.$i.'" value="" size="30" maxlength="30" />';
                $retour .= '<label for="lienMenuCreateB'.$i.'"> lien :</label><select name="lienMenu" id="lienMenuCreateB'.$i.'" style="width:100px">';
                $retour .= '<option value=""></option>';
                $req2 = Bdd::execute('SELECT nom FROM page ORDER BY nom', array());
                while ($donnees2 = $req2->fetch()) {
                    $retour .= '<option value="'.$donnees2['nom'].'">'.$donnees2['nom'].'</option>';
                }
                $req2->closeCursor();
                $retour .= '</select>';
                $retour .= '<input type="submit" value = "Creer" /></div></form>';
                $retour .= '</li>';
            }
        }
        $req->closeCursor();
        return $retour;
    }

    protected function listeActions() {
        $retour = array();
        $retour[] = 'Editer';
        return $retour;
    }

    protected function listeModules() {
        $retour = array();
        $retour[] = $this;
        return $retour;
    }
}

?>

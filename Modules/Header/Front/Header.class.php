<?php

class Header extends FrontOffice {

    protected $search;

    public function __construct($racineSite, $search) {
        parent::init('Header', $racineSite);

        $this->search = $search;
    }

    public function action() {
        switch($this->action) {
            default: break;
        }
    }

    public function affiche() {
        $retour = '<div id="header">';
        switch($this->affiche) {
            default: $retour .= $this->afficheDefaut(); break;
        }
        $retour .= $this->search->affiche();
        $retour .= '</div>';
        return $retour;
    }

    protected function afficheDefaut() {
        $req = Bdd::execute('SELECT * FROM headerbase', array());
        $donnees = $req->fetch();
        $req->closeCursor();
        return html_entity_decode($donnees['contenu-'.Utils::$GET['langue']]);
    }
}

?>

<?php

class Utils {

    public static function indent($html) {
        $stringIndented=NULL;
        $indexStringIndented=0;
        $indentationLevel=0;
        $size=strlen($html);
        for ($i=0;$i<$size;++$i) {
            $char=substr($html,$i,1);
            switch ($char) {
                case '<':
                    if (substr($html,$indexStringIndented+1,1)=='/') {
                        --$indentationLevel;
                        while(substr($html,$i,1)!='>') ++$i;
                        for ($j=0;$j<$indentationLevel;++$j) $stringIndented.="\t";
                        $stringIndented.=substr($html,$indexStringIndented,$i+1-$indexStringIndented);
                        $stringIndented.="\n";
                        $indexStringIndented=$i+1;
                    }
                    else if (substr($html,$indexStringIndented+1,1)!='/') {
                        while(substr($html,$i,1)!='>') ++$i;
                        if (substr($html,$i-1,1)!='/') {
                            for ($j=0;$j<$indentationLevel;++$j) $stringIndented.="\t";
                            $stringIndented.=substr($html,$indexStringIndented,$i+1-$indexStringIndented);
                            $stringIndented.="\n";
                            $indexStringIndented=$i+1;
                            ++$indentationLevel;
                        } else {
                            for ($j=0;$j<$indentationLevel;++$j) $stringIndented.="\t";
                            $stringIndented.=substr($html,$indexStringIndented,$i+1-$indexStringIndented);
                            $stringIndented.="\n";
                            $indexStringIndented=$i+1;
                        }
                    }
                    break;
                default:
                    if (substr($html,$i+1,1)=='<') {
                        for ($j=0;$j<$indentationLevel;++$j) $stringIndented.="\t";
                        $stringIndented.=substr($html,$indexStringIndented,$i+1-$indexStringIndented);
                        $stringIndented.="\n";
                        $indexStringIndented=$i+1;
                    }
            }
        }
        return $stringIndented;
    }

    public static function linearize($html) {
        $html = preg_replace('/[\r\n]*/', '', $html);
        $html = preg_replace('/[\r]*/', '', $html);
        $html = preg_replace('/[\n]*/', '', $html);
        $html = preg_replace('/>[\t]*/', '>', $html);
        $html = preg_replace('/>[\s]*/', '>', $html);
        $html = preg_replace('/<[\t]*/', '<', $html);
        $html = preg_replace('/<[\s]*/', '<', $html);
        return $html;
    }

    public static $GET;
    public static $POST;
    public static $SESSION;
    public static $COOKIE;

    public static function init($get, $post, $session, $cookie)
    {
        Utils::$GET = $get;
        Utils::$POST = $post;
        Utils::$SESSION = $session;
        Utils::$COOKIE = $cookie;
    }
}

?>

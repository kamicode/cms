<?php

class Bdd {

    private static $bdd = null;

    private function __construct() { }

    public static function pdo() {
        if(is_null(self::$bdd)) {
            try {
                $dsn = 'mysql:host='.DBASE_SERVER.';dbname='.DBASE_BASENAME.'';
                $user = DBASE_LOGIN;
                $password = DBASE_PASS;
                $extraParam= array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
                self::$bdd = new PDO($dsn, $user, $password, $extraParam);
                self::$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }
            catch(PDOException $e) {
                $msg = 'ERREUR PDO dans le fichier : '. $e->getFile();
                $msg += ' Ligne : ' . $e->getLine();
                $msg += ' : ' . $e->getMessage();
                echo $msg;
            } catch (Exception $e) {
                echo 'Erreur : '.$e->getMessage();
            }
        }
        return self::$bdd;
    }

    public static function execute($query, $parameters) {
        $ret = Bdd::pdo()->prepare($query);
        $ret->execute($parameters);
        return $ret;
    }

    public static function executerWithType($query, $parameters) {
        $ret = Bdd::pdo()->prepare($query);
        $count = count($parameters);
        for ($i = 0; $i < $count; $i++) {
            $ret->bindValue($i + 1, $parameters[$i][0], $parameters[$i][1]);
        }

        $ret->execute();
        return $ret;
    }
}

?>

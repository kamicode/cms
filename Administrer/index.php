<?php
header('Content-type: text/html; charset=UTF-8');

session_start();

class Main {
    public static function verifyLogon() {
        $salt = 'BwGk15l8WX';
        $admin = array (
                'Administrator' => md5('Elodie19'.$salt),
                'haiecapique' => md5('Elodie19'.$salt),
                //'Julien' => md5('JulienEpine65'.$salt),
                //'Gilles' => md5('GillesFuste'.$salt),
                //'Candice' => md5('CandicePyrenees65'.$salt),
                //'Guillaume' => md5('GuillaumeSpirit31'.$salt),
                //'Candice' => md5('CandiceSpirit65'.$salt),
        );

        if(Utils::POST && isset(Utils::$POST['logout'])) {
            unset($_SESSION['login']);
        }
        else if(Utils::$POST && isset(Utils::$POST['login']) && isset(Utils::$POST['mdp'])) {
            $password_md5 = md5(Utils::$POST['mdp'].$salt);
            if (array_key_exists(Utils::$POST['login'], $admin) && $admin[Utils::$POST['login']] == $password_md5) {
                Utils::$SESSION['login'] = Utils::$POST['login'];
            }
        }

        if(!isset(Utils::$SESSION['login'])) {
            return false;
        }
            return true;
    }

    public static function run() {
        require_once('../Utils.php');
        Utils::init($_GET, $_POST, $_SESSION, $_COOKIE);

        if(Main::verifyLogon()) {
            require_once('../Bdd/config.php');
            require_once('../Bdd/Bdd.class.php');
            require_once('../Modules/Module.class.php');
            require_once('../Modules/BackOffice.class.php');
            require_once('../Modules/Site/Back/Site.class.php');

            $site = new Site('../');
            $site->action();
            $html = $site->affiche();
            echo $html;
        }
        else
        {
            include("./form.html");
        }
    }
}

Main::run();

?>
